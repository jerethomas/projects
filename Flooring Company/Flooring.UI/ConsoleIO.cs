﻿using Flooring.BLL;
using Flooring.Models;
using Flooring.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Flooring.UI
{
    public class ConsoleIO
    {
        public static void DisplayOrderDetails(Order order, DateTime orderDate)
        {
            Regex comma = new Regex("[#]");
            order.CustomerName = comma.Replace(order.CustomerName, ",");

            Console.WriteLine($" Order Number: {order.OrderNumber} | Order Date: {orderDate:MM/dd/yyyy}");
            Console.WriteLine($"Customer Name: {order.CustomerName}    State: {order.State}");
            Console.WriteLine($" Product Type: {order.ProductType}");
            Console.WriteLine($"         Area: {order.Area}");
            Console.WriteLine($"Material Cost: {order.MaterialCost:c}");
            Console.WriteLine($"   Labor Cost: {order.LaborCost:c}");
            Console.WriteLine($"          Tax: {order.Tax:c}");
            Console.WriteLine($"  Order Total: {order.Total:c}");
        }

        public static bool CheckDate(string inputedOrderDate, OrderManager manager)
        {            
            DateTime outputOrderDate;
            bool isInformationValid = true;

            if (!DateTime.TryParse(inputedOrderDate, out outputOrderDate))
            {

                Console.WriteLine("Please input the date in the proper format.");
                isInformationValid = false;
                return false;                                  
            }

            if (isInformationValid)
            {
                bool checkIfDateIsValid = manager.LoadOrderRepositoryByDate(outputOrderDate);

                isInformationValid = checkIfDateIsValid;
            }

            if (!isInformationValid)
            {

                Console.WriteLine("There is no orders for the date given");
                Console.WriteLine("Please check your date and try again.");
                return false;
            }

            return true;      
        }

        public static bool IsDateInPast(string inputedOrderDate)
        {
            DateTime outputOrderDate;
            bool isInformationValid;

            if (DateTime.TryParse(inputedOrderDate, out outputOrderDate))
            {
                if (outputOrderDate < DateTime.Now)
                {
                    Console.WriteLine("The entered date can not be in the past");
                    isInformationValid = false;
                    return false;
                }
            }
            else
            {
                Console.WriteLine("Please input the date in the proper format.");
                isInformationValid = false;
                return false;
            }

            return true;
        }

        public static bool CheckOrderNumber(string inputedOrderNumber, OrderManager manager)
        {
            bool isInformationValid = true;
            int outputForOrderNumber;
            OrderLookupResponse retrieveOrder = new OrderLookupResponse();           

            if (!int.TryParse(inputedOrderNumber, out outputForOrderNumber))
            {                
                Console.WriteLine("The entered order Number is not valid.");
                isInformationValid = false;
                return false;
            }
            if (isInformationValid)
            {
                retrieveOrder = manager.LookupOrder(outputForOrderNumber);

                if (!retrieveOrder.Success)
                {
                    Console.WriteLine("There is no order for the given order number.");
                    isInformationValid = false;
                    return false;
                }
            }            

            return true;
        }

        public static string GetYesNoAnswerFromUser()
        {
            while (true)
            {
                Console.Write("(Y/N)? ");
                string input = Console.ReadLine().ToUpper();

                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("You must enter Y/N.");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                }
                else
                {
                    if (input != "Y" && input != "N")
                    {
                        Console.WriteLine("You must enter Y/N.");
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        continue;
                    }

                    return input;
                }
            }
        }

        public static decimal GetAreaFromUser()
        {
            decimal output;

            while (true)
            {
                Console.Write("Enter Area: ");

                string inputForArea = Console.ReadLine();

                if (inputForArea == "")
                {
                    return output = 0;
                }

                else if (!decimal.TryParse(inputForArea, out output))
                {
                    Console.WriteLine("You must enter a valid Area");
                }

                if (output < 100)
                {
                    Console.WriteLine("The Area can not be less than 100.");
                }
                else
                {
                    return output;
                }
            }            
        }
       
        public static string GetValidProductTypeFromUser(OrderManager manager)
        {
            var productList = manager.ProductList();

            Console.Write("Our list of products are: ");

            for (int i = 0; i < productList.Count; i++)
            {
                Console.Write($"{productList[i].ProductType}. ");
            }

            Console.WriteLine();
            Console.WriteLine();

            while (true)
            {                
                Console.Write("Enter the name of the product from the list above: ");
                string inputForProductType = Console.ReadLine();
                string fixedinputForProductType;

                if (string.IsNullOrEmpty(inputForProductType))
                {
                    return "";
                }
                else
                {
                    fixedinputForProductType = inputForProductType.First().ToString().ToUpper() + inputForProductType.Substring(1);

                    if (manager.LookupProduct(fixedinputForProductType))
                    {
                        return fixedinputForProductType;
                    }
                    else
                    {
                        Console.WriteLine("That is not a product we sell.");
                    }
                    
                }
            }
        }

        public static string GetValidCustomerNameFromUser()
        {
            while (true)
            {
                Console.Write("Enter the customer name: ");
                string inputForCustomerName = Console.ReadLine();
                Regex rg = new Regex("[^a-zA-Z0-9 ,.]");
                Regex comma = new Regex("[,]");

                if (string.IsNullOrEmpty(inputForCustomerName))
                {
                    return "";
                }

                else if (rg.IsMatch(inputForCustomerName))
                {
                    Console.WriteLine("Only alphanumeric [a-z][0-9] as well as periods and commas are allowed.");
                }

                else
                {
                    inputForCustomerName = comma.Replace(inputForCustomerName, "#");
                    return inputForCustomerName;
                }
            }
        }

        public static string GetValidStateFromUser(OrderManager manager)
        {
            while (true)
            {
                Console.Write("Enter the state: ");
                string inputForState = Console.ReadLine();

                if (string.IsNullOrEmpty(inputForState))
                {
                    return "";
                }
                else
                {
                    if (inputForState.Length == 2)
                    {
                        inputForState = inputForState.ToUpper();
                    }

                    else if (inputForState.Length > 2)
                    {
                        inputForState = inputForState.First().ToString().ToUpper() + inputForState.Substring(1);
                    }

                    if (manager.LookupState(inputForState))
                    {
                        return inputForState;
                    }
                    else
                    {
                        Console.WriteLine("We do not sell in that state.");
                    }                    
                }
            }
        }        

    }
}