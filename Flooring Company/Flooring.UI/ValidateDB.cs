﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI
{
    public class ValidateDB
    {

        public static bool validateFiles()
        {
            string taxFilePath = @"C:\Data\Flooring\Taxes.txt";
            string productFilePath = @"C:\Data\Flooring\Products.txt";

            if (!File.Exists(taxFilePath))
            {
                Console.WriteLine("The tax data file could not be found.");
                Console.WriteLine("Press any key to continue");
                Console.ReadLine();
                return false;
            }
            else if (!File.Exists(productFilePath))
            {
                Console.WriteLine("The product data file could not be found.");
                Console.WriteLine("Press any key to continue");
                Console.ReadLine();
                return false;
            }

            return true;
        }

    }
}
