﻿using Flooring.BLL;
using Flooring.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI.Workflows
{
    public class EditOrderWorkflow
    {
        public void Execute()
        {
            OrderManager manager = OrderManagerFactory.Create();

            bool isInformationValid = true;

            Console.Clear();
            Console.WriteLine("Edit an Order");
            Console.WriteLine("--------------------------");

            string dateInput;
            DateTime orderDate = new DateTime();
            bool keepLooping = true;

            while (keepLooping)
            {
                Console.Write("Enter a Date (Q to quit): ");
                dateInput = Console.ReadLine();

                if (dateInput == "q" || dateInput == "Q")
                {
                    isInformationValid = false;
                    keepLooping = false;
                    break;
                }

                if (ConsoleIO.CheckDate(dateInput, manager))
                {
                    orderDate = DateTime.Parse(dateInput);
                    keepLooping = false;
                    isInformationValid = true;
                }

            }

            int inputForOrderNumber = 0;
            string stringInputForOrderNumber = null;

            OrderLookupResponse retrieveOrder = new OrderLookupResponse();

            if (isInformationValid)
            {
                keepLooping = true;
            }            

            while (keepLooping)
            {
                if (isInformationValid)
                {
                    Console.Write("Enter Order Number (Q to quit): ");
                    stringInputForOrderNumber = Console.ReadLine();

                    if (stringInputForOrderNumber == "q" || stringInputForOrderNumber == "Q")
                    {
                        isInformationValid = false;
                    keepLooping = false;
                    break;
                    }

                    if (ConsoleIO.CheckOrderNumber(stringInputForOrderNumber, manager))
                    {
                        keepLooping = false;
                        isInformationValid = true;
                    }
                }
            }           

            if (isInformationValid)
            {
                inputForOrderNumber = int.Parse(stringInputForOrderNumber);
                retrieveOrder = manager.LookupOrder(inputForOrderNumber);

                ConsoleIO.DisplayOrderDetails(retrieveOrder.Order, orderDate);

                Console.Write("Is this the order you want to edit ");
                string yesOrNo = ConsoleIO.GetYesNoAnswerFromUser();

                if (yesOrNo != "Y")
                {
                    isInformationValid = false;
                }
            }

            if (isInformationValid)
            {
                Console.WriteLine();
                Console.WriteLine("Instructions: to leave a field the same as it is in the order above, simply hit \"Enter\" to move to the next field.");
                Console.WriteLine();

                Console.WriteLine($"Current customer name ({retrieveOrder.Order.CustomerName}).");
                string inputForCustomerName = ConsoleIO.GetValidCustomerNameFromUser();

                Console.WriteLine($"Current state ({retrieveOrder.Order.State}).");
                string inputForState = ConsoleIO.GetValidStateFromUser(manager);

                Console.WriteLine($"Current product type ({retrieveOrder.Order.ProductType}).");
                string inputForProductType = ConsoleIO.GetValidProductTypeFromUser(manager);

                Console.WriteLine($"Current area ({retrieveOrder.Order.Area}).");
                decimal InputForArea = ConsoleIO.GetAreaFromUser();
                
                OrderEditResponse response = manager.EditOrder(
                    orderDate, inputForOrderNumber, inputForCustomerName, inputForState, inputForProductType, InputForArea);

                string finalYesNoFromUser = null;

                if (response.Success)
                {
                    Console.WriteLine();
                    ConsoleIO.DisplayOrderDetails(response.Order, orderDate);
                    Console.Write("Do you want to save this order?");
                    finalYesNoFromUser = ConsoleIO.GetYesNoAnswerFromUser();
                }
                                
                if (finalYesNoFromUser == "Y")
                {
                    manager.SaveOrder(response.Order, orderDate);
                    Console.WriteLine();
                    Console.WriteLine("The order has been saved!");
                    Console.WriteLine();
                }

                else
                {
                    Console.WriteLine("An error occurred: ");
                    Console.WriteLine(response.Message);
                }
            }                  
                    
            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}