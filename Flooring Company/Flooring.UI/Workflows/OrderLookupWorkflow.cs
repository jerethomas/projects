﻿using Flooring.BLL;
using Flooring.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI.Workflows
{
    public class OrderLookupWorkflow
    {
        public void Execute()
        {
            OrderManager manager = OrderManagerFactory.Create();

            bool isInformationValid = true;         

            Console.Clear();
            Console.WriteLine("Lookup an Order");
            Console.WriteLine("--------------------------");

            string dateInput;
            DateTime orderDate = new DateTime();
            bool keepLooping = true;

            while (keepLooping)
            {
                Console.Write("Enter a Date (Q to quit): ");
                dateInput = Console.ReadLine();

                if (dateInput == "q" || dateInput == "Q")
                {
                    isInformationValid = false;
                    keepLooping = false;
                    break;
                }

                if (ConsoleIO.CheckDate(dateInput, manager))
                {
                    orderDate = DateTime.Parse(dateInput);
                    keepLooping = false;
                    isInformationValid = true;
                }

            }

            int inputForOrderNumber = 0;
            string stringInputForOrderNumber = null;

            OrderLookupResponse retrieveOrder = new OrderLookupResponse();

            if (isInformationValid)
            {
                keepLooping = true;
            }

            while (keepLooping)
            {
                if (isInformationValid)
                {
                    Console.Write("Enter Order Number (Q to quit): ");
                    stringInputForOrderNumber = Console.ReadLine();

                    if (stringInputForOrderNumber == "q" || stringInputForOrderNumber == "Q")
                    {
                        isInformationValid = false;
                        keepLooping = false;
                        break;
                    }

                    if (ConsoleIO.CheckOrderNumber(stringInputForOrderNumber, manager))
                    {
                        keepLooping = false;
                        isInformationValid = true;
                    }
                }
            }

            if (isInformationValid)
            {

                inputForOrderNumber = int.Parse(stringInputForOrderNumber);
                retrieveOrder = manager.LookupOrder(inputForOrderNumber);

                if (retrieveOrder.Success)
                {
                    ConsoleIO.DisplayOrderDetails(retrieveOrder.Order, orderDate);
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("An error occurred: ");
                    Console.WriteLine(retrieveOrder.Message);
                }
            }            

            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
