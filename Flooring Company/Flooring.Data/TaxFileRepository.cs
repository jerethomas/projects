﻿using Flooring.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Data
{
    public class TaxFileRepository
    {
        private string _filePath = @"C:\Data\Flooring\Taxes.txt";

        public bool doesFileExsist()
        {


            return true;
        }

        private List<Tax> taxList;

        private void loadTaxFile()
        {
            taxList = new List<Tax>();

            using (StreamReader sr = new StreamReader(_filePath))
            {
                sr.ReadLine();
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    Tax newTaxList = new Tax();

                    string[] colums = line.Split(',');

                    newTaxList.StateAbbreviation = colums[0];
                    newTaxList.StateName = colums[1];
                    newTaxList.TaxRate = decimal.Parse(colums[2]);

                    taxList.Add(newTaxList);
                }
            }
        }

        public List<Tax> RetriveTaxDataList()
        {
            loadTaxFile();

            return taxList;
        }

        public Tax RetriveTaxData(string state)
        {
            loadTaxFile();

            var taxFromAbbreviation = taxList.Where(t => t.StateAbbreviation == state);

            foreach (Tax loadTax in taxFromAbbreviation)
            {
                return loadTax;
            }

            if (taxFromAbbreviation.Count() == 0)
            {
                var taxWithFullStateName = taxList.Where(t => t.StateName == state);

                foreach (Tax loadTax in taxWithFullStateName)
                {
                    return loadTax;
                }
            }

            return null;
        }
    }
}
