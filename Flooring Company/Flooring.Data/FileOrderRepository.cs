﻿using Flooring.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Models;
using System.IO;

namespace Flooring.Data
{
    public class FileOrderRepository : IOrderRepository
    {
        private DateTime _orderDate;
      
        public void SetOrderDate(DateTime orderDate)
        {
            _orderDate = orderDate;
        }

        private string _filePath
        {
            get { return $"C:\\Data\\Flooring\\Orders_{_orderDate:MMddyyyy}.txt"; }
        }

        public bool DoesFileExist()
        {
            bool fileExist = false;

            if (File.Exists(_filePath))
            {
                fileExist = true;
            }

            return fileExist;
        }

        private void CreateFileIfFileDoesNotExist()
        {
            if (!File.Exists(_filePath))
            {
                using (StreamWriter writer = File.AppendText($"C:\\Data\\Flooring\\Orders_{_orderDate:MMddyyyy}.txt"))
                {
                    writer.WriteLine("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");
                }
            }
        }

        private List<Order> orderList; 
        public void LoadOrders()
        {
            CreateFileIfFileDoesNotExist();

            orderList = new List<Order>();

            using ( StreamReader sr = new StreamReader( _filePath ) )
            {
                sr.ReadLine();
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    Order newOrder = new Order();

                    string[] colums = line.Split(',');

                    newOrder.OrderNumber = int.Parse(colums[0]);
                    newOrder.CustomerName = colums[1];
                    newOrder.State = colums[2];
                    newOrder.TaxRate = decimal.Parse(colums[3]);
                    newOrder.ProductType = colums[4];
                    newOrder.Area = decimal.Parse(colums[5]);
                    newOrder.CostPerSquareFoot = decimal.Parse(colums[6]);
                    newOrder.LaborCostPerSquareFoot = decimal.Parse(colums[7]);
                    newOrder.MaterialCost = decimal.Parse(colums[8]);
                    newOrder.LaborCost = decimal.Parse(colums[9]);
                    newOrder.Tax = decimal.Parse(colums[10]);
                    newOrder.Total = decimal.Parse(colums[11]);

                    orderList.Add(newOrder);
                }
            }
        }

        public List<Order> RetriveOrderList()
        {
            LoadOrders();

            return orderList;
        }

        public Order RetriveOrder(int OrderNumber)
        {
            LoadOrders();            

            var order = orderList.Where(o => o.OrderNumber == OrderNumber);                      

            foreach (Order loadOrder in order)
            {
                return loadOrder;
            }

            return null;
        }

        public void AddOrder(Order order)
        {
            CreateFileIfFileDoesNotExist();

            using (StreamWriter sw = new StreamWriter(_filePath, true))
            {
                string line = CreateCsvForOrder(order);

                sw.WriteLine(line);
            }
        }

        public void SaveOrder(Order order)
        {
            var orderToEdit = orderList.FindIndex(o => o.OrderNumber == order.OrderNumber);

            orderList[orderToEdit] = order;

            CreateOrderFile(orderList);
        }

        public void RemoveOrder(Order order)
        {
            //var orderToRemove = orderList.FindIndex(o => o.OrderNumber == order.OrderNumber);

            orderList.Remove(order);

            CreateOrderFile(orderList);
        }

        private string CreateCsvForOrder(Order order)
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", order.OrderNumber, order.CustomerName, order.State,
                order.TaxRate, order.ProductType, order.Area, order.CostPerSquareFoot, order.LaborCostPerSquareFoot, order.MaterialCost, order.LaborCost, order.Tax, order.Total);
        }

        private void CreateOrderFile (List<Order> orders)
        {
            if (File.Exists(_filePath))
            {
                File.Delete(_filePath);
            }

            using (StreamWriter sr = new StreamWriter(_filePath))
            {
                sr.WriteLine("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");
                
                foreach(var order in orders)
                {
                    sr.WriteLine(CreateCsvForOrder(order));
                }
            }
        }


    }
}