﻿using Flooring.Data;
using Flooring.Models;
using Flooring.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.BLL
{
    public class RemoveOrderRules
    {
        public OrderRemoveResponse RemoveOrder (DateTime orderDate, Order order)
        {
            OrderRemoveResponse response = new OrderRemoveResponse();

            response.Order = order;

            FileOrderRepository retriveOrder = new FileOrderRepository();
            retriveOrder.SetOrderDate(orderDate);



            return response;
        }
    }
}
