﻿using Flooring.Data;
using Flooring.Models;
using Flooring.Models.Interfaces;
using Flooring.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.BLL
{
    public class AddOrderRules
    {
        public bool CheckUserInputedDate(DateTime orderDate)
        {
            OrderAddResponse response = new OrderAddResponse();

            if (orderDate < DateTime.Today)
            {
                Console.WriteLine("The date for the new Order can not be in the past.");
                return false;
            }

            else
            {
                return true;
            }
        }

        public OrderAddResponse AddOrder(Order order, DateTime orderDate)
        {
            FileOrderRepository retriveOrderNumber = new FileOrderRepository();
            retriveOrderNumber.SetOrderDate(orderDate);

            OrderAddResponse response = new OrderAddResponse();

            TaxFileRepository loadTaxData = new TaxFileRepository();

            ProductFileRepository loadProductData = new ProductFileRepository();

            List<Order> orderList = retriveOrderNumber.RetriveOrderList();

            var productData = loadProductData.RetriveProducts(order.ProductType);

            var taxData = loadTaxData.RetriveTaxData(order.State);

            if (taxData == null)
            {
                response.Success = false;
                Console.WriteLine("We can not sell in that state.");
                return response;
            }

            if (productData == null)
            {
                response.Success = false;
                Console.WriteLine("The product type entered is not somthing we sell.");
                return response;
            }
            
            response.Success = true;
            response.Order = order;

            if (orderList.Count <= 0)
            {
                response.Order.OrderNumber = 1;                
            }
            else
            {
                response.Order.OrderNumber = orderList.Max(o => o.OrderNumber) + 1;
            }
            
            response.Order.TaxRate = taxData.TaxRate;
            response.Order.CostPerSquareFoot = productData.CostPerSquareFoot;
            response.Order.LaborCostPerSquareFoot = productData.LaborCostPerSquareFoot;
            response.Order.MaterialCost = response.Order.Area * response.Order.CostPerSquareFoot;
            response.Order.LaborCost = response.Order.Area * response.Order.LaborCostPerSquareFoot;
            response.Order.Tax = (response.Order.MaterialCost + response.Order.LaborCost) * (response.Order.TaxRate / 100);
            response.Order.Total = response.Order.MaterialCost + response.Order.LaborCost + response.Order.Tax;

            return response;
        }
    }
}
