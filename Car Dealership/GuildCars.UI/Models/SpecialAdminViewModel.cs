﻿using GuildCars.Models.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GuildCars.UI.Models
{
    public class SpecialAdminViewModel: IValidatableObject
    {
        public Special Special { get; set; }
        public IEnumerable<Special> SpecialList { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (ImageUpload.ContentLength < 0 || ImageUpload == null)
            {
                errors.Add(new ValidationResult("An Image of the vehicle is required."));
            }

            if (string.IsNullOrEmpty(Special.SpecialName))
            {
                errors.Add(new ValidationResult("The name of the special is required."));
            }

            if (string.IsNullOrEmpty(Special.SpecialDescription))
            {
                errors.Add(new ValidationResult("A description of the special is required."));
            }

            return errors;
        }
    }
}