﻿using GuildCars.Models.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuildCars.UI.Models
{
    public class InventoryReportsViewModel
    {
        public IEnumerable<NewVehicleReportItem> NewVehicleReportItem { get; set; }
        public IEnumerable<UsedVehicleReportItem> UsedVehicleReportItem { get; set; }
    }
}