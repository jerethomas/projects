﻿using GuildCars.Models.Queries;
using GuildCars.Models.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace GuildCars.UI.Models
{
    public class PurchaseViewModel: IValidatableObject
    {
        public VehicleDetailItem Vehicle { get; set; }
        public IEnumerable<State> States { get; set; }
        public IEnumerable<PurchaseType> PurchaseType { get; set; }
        public Purchase Purchase { get; set; }

        public static bool IsPhoneNumber(string Phone)
        {
            if (Regex.Match(Phone, @"1?[\s.-]?\(?(\d{3})\)?[\s.-]?\d{3}[\s.-]?\d{4}").Success)
            {
                return true;
            }

            return false;
        }

        public static bool IsEmail(string Email)
        {
            if (Regex.Match(Email, @"(.*)@(.*)\.(.*)").Success)
            {
                return true;
            }

            return false;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(Purchase.Email) && (string.IsNullOrEmpty(Purchase.Phone)))
            {
                errors.Add(new ValidationResult("A Phone number or Email address must be provided."));
            }

            bool validPhoneNumber = false;
            bool validEmail = false;

            if (!string.IsNullOrEmpty(Purchase.Phone))
            {
                validPhoneNumber = IsPhoneNumber(Purchase.Phone);
            }

            if (!string.IsNullOrEmpty(Purchase.Email))
            {
                validEmail = IsEmail(Purchase.Email);
            }

            if (!validPhoneNumber)
            {
                errors.Add(new ValidationResult("Please enter a valid phone number."));
            }

            if (!validEmail)
            {
                errors.Add(new ValidationResult("Please enter a valid Email."));
            }

            if (!validPhoneNumber && string.IsNullOrEmpty(Purchase.Email))
            {
                errors.Add(new ValidationResult("Please enter a valid phone number or put in an email address."));
            }

            if (!validEmail && string.IsNullOrEmpty(Purchase.Phone))
            {
                errors.Add(new ValidationResult("Please enter a valid Email or put in a phone number."));
            }

            if (string.IsNullOrEmpty(Purchase.PurchaseName))
            {
                errors.Add(new ValidationResult("Please enter a valid Name for purchase."));
            }

            if (string.IsNullOrEmpty(Purchase.Street1))
            {
                errors.Add(new ValidationResult("Please enter a valid street address for purchase."));
            }

            if (string.IsNullOrEmpty(Purchase.City))
            {
                errors.Add(new ValidationResult("Please enter a valid City for purchase."));
            }

            if (Purchase.Zipcode <= 9999.99m)
            {
                errors.Add(new ValidationResult("Please enter a valid Zipcode for purchase."));
            }

            if (Purchase.PurchasePrice < Vehicle.Price - (Vehicle.Price * 5 / 100))
            {
                errors.Add(new ValidationResult("The purchase price cannot be less than 95% of the advertised price."));
            }

            if (Purchase.PurchasePrice > Vehicle.MSRP)
            {
                errors.Add(new ValidationResult("The purchase price cannot be larger than the MSRP."));
            }

            return errors;
        }
    }
}