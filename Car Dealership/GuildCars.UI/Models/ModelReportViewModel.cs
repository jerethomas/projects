﻿using GuildCars.Models.Queries;
using GuildCars.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuildCars.UI.Models
{
    public class ModelReportViewModel
    {
        public string ModelName { get; set; }
        public IEnumerable<ModelReportItem> ModelList { get; set; }
        public IEnumerable<Make> Makes { get; set; }
    }
}