namespace GuildCars.UI.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GuildCars.UI.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(GuildCars.UI.Models.ApplicationDbContext context)
        {
            var userMgr = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleMgr = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(context));

            // have we loaded roles already?
            if (!roleMgr.RoleExists("admin"))
            {
                roleMgr.Create(new ApplicationRole() { Name = "admin" });
            }                

            // create the admin role 
            

            if (!roleMgr.RoleExists("sales"))
            {
                roleMgr.Create(new ApplicationRole() { Name = "sales" });
            }                

            

            if (!roleMgr.RoleExists("disable"))
            {
                roleMgr.Create(new ApplicationRole() { Name = "disable" });
            }                           

            // create the default user 
            var user = new ApplicationUser()
            {
                UserName = "admin@admin.com",
                Email = "admin@admin.com",
                FirstName = "test",
                LastName = "admin"
            };
            // create the user with the manager class 
            userMgr.Create(user, "testing123");
            // add the user to the admin role 
            userMgr.AddToRole(user.Id, "admin");
        }

    }
}