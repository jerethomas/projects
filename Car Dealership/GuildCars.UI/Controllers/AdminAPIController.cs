﻿using GuildCars.Data.Factories;
using GuildCars.Models.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GuildCars.UI.Controllers
{
    public class AdminAPIController : ApiController
    {
        [Authorize(Roles = "admin")]
        [Route("admin/vehicle/search")]
        [AcceptVerbs("GET")]
        public IHttpActionResult SearchNew(string quickSearch, decimal? minYear, decimal? maxYear, decimal? minPrice, decimal? maxPrice)
        {
            var repo = VehiclesRepositoryFactory.GetRepository();

            try
            {
                var parameters = new VehicleSearchParameters()
                {
                    QuickSearch = quickSearch,
                    MinYear = minYear,
                    MaxYear = maxYear,
                    MinPrice = minPrice,
                    MaxPrice = maxPrice
                };

                var result = repo.VehicleSearchAll(parameters);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "admin")]
        [Route("admin/getmodels/{makeId}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetModels(int makeId)
        {
            var repo = ModelsRepositoryFactory.GetRepository();

            try
            {
                var result = repo.GetModelByMake(makeId);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "admin")]
        [Route("admin/salesreport/search")]
        [AcceptVerbs("GET")]
        public IHttpActionResult SalesReport(string UserId, string FromDate, string ToDate)
        {
            var repo = ReportsRepositoryFactory.GetRepository();

            try
            {
                var parameters = new SalesSearchParameters()
                {
                    UserId = UserId,
                    FromDate = FromDate,
                    ToDate = ToDate
                };

                var result = repo.SalesReport(parameters);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
