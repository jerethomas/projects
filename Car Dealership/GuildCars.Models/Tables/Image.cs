﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildCars.Models.Tables
{
    public class Image
    {
        public int ImageId { get; set; }
        public int VehicleId { get; set; }
        public string ImageFileName { get; set; }
    }
}
