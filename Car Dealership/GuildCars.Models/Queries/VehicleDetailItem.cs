﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildCars.Models.Queries
{
    public class VehicleDetailItem
    {
        public int VehicleId { get; set; }
        public int MakeId { get; set; }
        public string MakeName { get; set; }
        public int ModelId { get; set; }
        public string ModelName { get; set; }
        public int BodyStyleId { get; set; }
        public string BodyStyleName { get; set; }
        public string TransmissionId { get; set; }
        public string TransmissionName { get; set; }
        public int ExteriorColorId { get; set; }
        public string ExteriorColorName { get; set; }
        public int InteriorId { get; set; }
        public string InteriorName { get; set; }
        public decimal VehicleYear { get; set; }
        public decimal Milage { get; set; }
        public string VinNumber { get; set; }
        public decimal Price { get; set; }
        public decimal MSRP { get; set; }
        public string ListingDescription { get; set; }
        public string ImageFileName { get; set; }
        public int? PurchaseId { get; set; }
    }
}
