Use GuildCars
GO


IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'MakesSelectAll')
		DROP PROCEDURE MakesSelectAll
GO

CREATE PROCEDURE MakesSelectAll AS
BEGIN
	SELECT MakeId, MakeName, CreatedDate, UserId
	FROM Makes

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'MakesReport')
		DROP PROCEDURE MakesReport
GO

CREATE PROCEDURE MakesReport AS
BEGIN
	SELECT MakeId, MakeName, CreatedDate, Email		
	FROM Makes ma
		INNER JOIN AspNetUsers aspu ON ma.UserId = aspu.Id

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'ModelsReport')
		DROP PROCEDURE ModelsReport
GO

CREATE PROCEDURE ModelsReport AS
BEGIN
	SELECT ModelId, ModelName, MakeName, mo.CreatedDate, Email		
	FROM Models mo
		INNER JOIN Makes ma ON mo.MakeId = ma.MakeId
		INNER JOIN AspNetUsers aspu ON mo.UserId = aspu.Id
	ORDER BY MakeName

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'ModelsSelectAll')
		DROP PROCEDURE ModelsSelectAll
GO

CREATE PROCEDURE ModelsSelectAll AS
BEGIN
	SELECT ModelId, MakeId, ModelName, CreatedDate, UserId
	FROM Models

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'BodyStylesSelectAll')
		DROP PROCEDURE BodyStylesSelectAll
GO

CREATE PROCEDURE BodyStylesSelectAll AS
BEGIN
	SELECT BodyStyleId, BodyStyleName
	FROM BodyStyles

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'TransmissionsSelectAll')
		DROP PROCEDURE TransmissionsSelectAll
GO

CREATE PROCEDURE TransmissionsSelectAll AS
BEGIN
	SELECT TransmissionId, TransmissionName
	FROM Transmissions

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'ExteriorColorsSelectAll')
		DROP PROCEDURE ExteriorColorsSelectAll
GO

CREATE PROCEDURE ExteriorColorsSelectAll AS
BEGIN
	SELECT ExteriorColorId, ExteriorColorName
	FROM ExteriorColors

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'VehicleTypesSelectAll')
		DROP PROCEDURE VehicleTypesSelectAll
GO

CREATE PROCEDURE VehicleTypesSelectAll AS
BEGIN
	SELECT VehicleTypeId, VehicleTypeName
	FROM VehicleTypes

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'InteriorsSelectAll')
		DROP PROCEDURE InteriorsSelectAll
GO

CREATE PROCEDURE InteriorsSelectAll AS
BEGIN
	SELECT InteriorId, InteriorName
	FROM Interiors

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'PurchasesSelectAll')
		DROP PROCEDURE PurchasesSelectAll
GO

CREATE PROCEDURE PurchasesSelectAll AS
BEGIN
	SELECT PurchaseId, VehicleId, PurchaseName, Phone, Email, Street1, Street2, City, StateId, Zipcode, PurchasePrice, PurchaseTypeId, PurchaseDate, UserId
	FROM Purchases

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'PurchaseTypesSelectAll')
		DROP PROCEDURE PurchaseTypesSelectAll
GO

CREATE PROCEDURE PurchaseTypesSelectAll AS
BEGIN
	SELECT PurchaseTypeId, PurchaseTypeName
	FROM PurchaseTypes

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'StatesSelectAll')
		DROP PROCEDURE StatesSelectAll
GO

CREATE PROCEDURE StatesSelectAll AS
BEGIN
	SELECT StateId, StateName
	FROM States

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'SpecialsSelectAll')
		DROP PROCEDURE SpecialsSelectAll
GO

CREATE PROCEDURE SpecialsSelectAll AS
BEGIN
	SELECT SpecialId, SpecialName, SpecialDescription, SepcialImageFileName
	FROM Specials
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'FeaturedVehiclesSelect')
		DROP PROCEDURE FeaturedVehiclesSelect
GO

CREATE PROCEDURE FeaturedVehiclesSelect AS
BEGIN
	SELECT TOP 20 v.VehicleId, MakeName, ModelName, VehicleYear, Price, ImageFileName, VinNumber, FeatureVehicle
	FROM Vehicles v
		INNER JOIN Makes ma ON v.MakeId = ma.MakeId
		INNER JOIN Models mo ON v.ModelId = mo.ModelId
		FULL OUTER JOIN VehicleImages vi ON v.VehicleId = vi.VehicleId
		FULL OUTER JOIN Purchases pu ON v.VehicleId = pu.VehicleId
	WHERE FeatureVehicle = 1 AND PurchaseId IS NULL
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'GetModelByMake')
		DROP PROCEDURE GetModelByMake
GO

CREATE PROCEDURE GetModelByMake (
	@MakeId int
)AS
BEGIN
	SELECT ModelId, ModelName
	FROM Models mo
		INNER JOIN Makes ma ON mo.MakeId = ma.MakeId
	WHERE mo.MakeId = @MakeId
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'VehiclesSelectDetails')
		DROP PROCEDURE VehiclesSelectDetails
GO

CREATE PROCEDURE VehiclesSelectDetails (
	@VinNumber varchar(17)
)AS
BEGIN
	SELECT  v.VehicleId, ma.MakeId, MakeName, mo.ModelId, ModelName, bo.BodyStyleId, BodyStyleName, 
	v.TransmissionId, TransmissionName, v.ExteriorColorId, ExteriorColorName, v.InteriorId, InteriorName, 
	VehicleYear, Milage, VinNumber, Price, MSRP, ListingDescription, PurchaseId, ImageFileName 
	FROM Vehicles v
		INNER JOIN Makes ma ON v.MakeId = ma.MakeId
		INNER JOIN Models mo ON v.ModelId = mo.ModelId
		INNER JOIN BodyStyles bo ON v.BodyStyleId = bo.BodyStyleId
		INNER JOIN Transmissions tr ON v.TransmissionId = tr.TransmissionId
		INNER JOIN ExteriorColors ec ON v.ExteriorColorId = ec.ExteriorColorId
		INNER JOIN Interiors ic ON v.InteriorId = ic.InteriorId
		FULL OUTER JOIN VehicleImages vi ON v.VehicleId = vi.VehicleId
		FULL OUTER JOIN Purchases pu ON v.VehicleId = pu.VehicleId
	WHERE VinNumber = @VinNumber
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'GetVehicleToEdit')
		DROP PROCEDURE GetVehicleToEdit
GO

CREATE PROCEDURE GetVehicleToEdit (
	@VinNumber varchar(17)
)AS
BEGIN
	SELECT  v.VehicleId, v.MakeId, v.ModelId, v.BodyStyleId, v.TransmissionId, v.ExteriorColorId, v.InteriorId, VehicleTypeId,
	VehicleYear, Milage, VinNumber, Price, MSRP, ListingDescription, ImageFileName, FeatureVehicle
	FROM Vehicles v		
		FULL OUTER JOIN VehicleImages vi ON v.VehicleId = vi.VehicleId
	WHERE VinNumber = @VinNumber
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'AddMakes')
		DROP PROCEDURE AddMakes
GO

CREATE PROCEDURE AddMakes (
	@MakeId int output,
	@MakeName varchar(25),
	@CreatedDate datetime2,
	@UserId nvarchar(128)
)AS
BEGIN
	INSERT INTO Makes (MakeName, CreatedDate, UserId)
	VALUES (@MakeName, @CreatedDate, @UserId)

	SET @MakeId = SCOPE_IDENTITY();
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'AddModels')
		DROP PROCEDURE AddModels
GO

CREATE PROCEDURE AddModels (
	@ModelId int output,
	@MakeId int,
	@ModelName varchar(25),
	@CreatedDate datetime2,
	@UserId nvarchar(128)
)AS
BEGIN
	INSERT INTO Models(MakeId, ModelName, CreatedDate, UserId)
	VALUES (@MakeId, @ModelName, @CreatedDate, @UserId)

	SET @ModelId = SCOPE_IDENTITY();
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'AddSpecials')
		DROP PROCEDURE AddSpecials
GO

CREATE PROCEDURE AddSpecials (
	@SpecialId int output,
	@SpecialName varchar(50),
	@SpecialDescription varchar(500),
	@SepcialImageFileName varchar(50)
)AS
BEGIN
	INSERT INTO Specials (SpecialName, SpecialDescription, SepcialImageFileName)
	VALUES (@SpecialName, @SpecialDescription, @SepcialImageFileName)

	SET @SpecialId = SCOPE_IDENTITY();
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'AddContacts')
		DROP PROCEDURE AddContacts
GO

CREATE PROCEDURE AddContacts (
	@ContactId int output,
	@ContactName varchar(50),
	@ContactEmail varchar(50),
	@ContactPhone varchar(18),
	@ContactMessage varchar(500)
)AS
BEGIN
	INSERT INTO Contacts (ContactName, ContactEmail, ContactPhone, ContactMessage)
	VALUES (@ContactName, @ContactEmail, @ContactPhone, @ContactMessage)

	SET @ContactId = SCOPE_IDENTITY();
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'AddPurchase')
		DROP PROCEDURE AddPurchase
GO

CREATE PROCEDURE AddPurchase (
	@PurchaseId int output,
	@VehicleId int,
	@PurchaseName varchar(50),
	@Phone varchar(18),
	@Email varchar(50),
	@Street1 varchar(50),
	@Street2 varchar(50),
	@City varchar(50),
	@StateId varchar(2),
	@Zipcode decimal(5),
	@PurchasePrice decimal(8,2),
	@PurchaseTypeId char(1),
	@PurchaseDate datetime2,
	@UserId nvarchar(128)
)AS
BEGIN
	INSERT INTO Purchases (VehicleId, PurchaseName, Phone, Email, Street1, Street2, City, StateId, Zipcode, PurchasePrice, PurchaseTypeId, PurchaseDate, UserId)
	VALUES (@VehicleId, @PurchaseName, @Phone, @Email, @Street1, @Street2, @City, @StateId, @Zipcode, @PurchasePrice, @PurchaseTypeId, @PurchaseDate, @UserId)

	SET @PurchaseId = SCOPE_IDENTITY();
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'AddVehicleImages')
		DROP PROCEDURE AddVehicleImages
GO

CREATE PROCEDURE AddVehicleImages (
	@ImageId int output,
	@VehicleId int,
	@ImageFileName varchar(50)

)AS
BEGIN
	INSERT INTO VehicleImages (VehicleId, ImageFileName)
	VALUES (@VehicleId, @ImageFileName)

	SET @ImageId = SCOPE_IDENTITY();
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'AddVehicles')
		DROP PROCEDURE AddVehicles
GO

CREATE PROCEDURE AddVehicles (
	@VehicleId int output,
	@MakeId int,
	@ModelId int,
	@BodyStyleId int ,
	@TransmissionId char(1),
	@ExteriorColorId int,
	@InteriorId int,
	@VehicleTypeId char(1),
	@VehicleYear decimal(4),
	@Milage decimal(9,2),
	@VinNumber varchar(17),
	@Price decimal(8,2),
	@MSRP decimal(8,2),
	@CreateDate datetime2,
	@ListingDescription varchar(500),
	@FeatureVehicle bit
)AS
BEGIN
	INSERT INTO Vehicles (MakeId, ModelId, BodyStyleId, TransmissionId, ExteriorColorId, InteriorId, 
	VehicleTypeId, VehicleYear, Milage, VinNumber, Price, MSRP, CreateDate, ListingDescription, FeatureVehicle)
	VALUES (@MakeId, @ModelId, @BodyStyleId, @TransmissionId, @ExteriorColorId, @InteriorId, 
	@VehicleTypeId, @VehicleYear, @Milage, @VinNumber, @Price, @MSRP, @CreateDate, @ListingDescription, @FeatureVehicle)

	SET @VehicleId = SCOPE_IDENTITY();
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'EditVehicles')
		DROP PROCEDURE EditVehicles
GO

CREATE PROCEDURE EditVehicles (
	@VehicleId int,
	@MakeId int,
	@ModelId int,
	@BodyStyleId int ,
	@TransmissionId char(1),
	@ExteriorColorId int,
	@InteriorId int,
	@VehicleTypeId char(1),
	@VehicleYear decimal(4),
	@Milage decimal(9,2),
	@VinNumber varchar(17),
	@Price decimal(8,2),
	@MSRP decimal(8,2),
	@ListingDescription varchar(500),
	@FeatureVehicle bit,
	@ImageFileName varchar(50)
)AS
BEGIN
	UPDATE Vehicles SET 
	MakeId = @MakeId,
	ModelId = @ModelId,
	BodyStyleId = @BodyStyleId,
	TransmissionId = @TransmissionId, 
	ExteriorColorId = @ExteriorColorId, 
	InteriorId = @InteriorId, 
	VehicleTypeId = @VehicleTypeId, 
	VehicleYear = @VehicleYear, 
	Milage = @Milage, 
	VinNumber = @VinNumber, 
	Price = @Price, 
	MSRP = @MSRP,
	ListingDescription = @ListingDescription, 
	FeatureVehicle = @FeatureVehicle

	WHERE VehicleId = @VehicleId

	UPDATE VehicleImages SET
	ImageFileName = @ImageFileName
	WHERE VehicleId = @VehicleId

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'DeleteSpecials')
		DROP PROCEDURE DeleteSpecials
GO

CREATE PROCEDURE DeleteSpecials (
	@SpecialId int
)AS
BEGIN
	BEGIN TRANSACTION

	DELETE FROM Specials 
	WHERE SpecialId = @SpecialId

	COMMIT TRANSACTION
	
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'DeleteVehicles')
      DROP PROCEDURE DeleteVehicles
GO

CREATE PROCEDURE DeleteVehicles (
	@VehicleId int
) AS
BEGIN
	BEGIN TRANSACTION

	DELETE FROM VehicleImages WHERE VehicleId = @VehicleId;
	DELETE FROM Vehicles WHERE VehicleId = @VehicleId;

	COMMIT TRANSACTION
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'NewInventoryReport')
      DROP PROCEDURE NewInventoryReport
GO

CREATE PROCEDURE NewInventoryReport AS
BEGIN

	SELECT VehicleYear, MakeName, ModelName,
		 COUNT (*) AS CountTotal,
		 SUM (MSRP) AS StockValue
	FROM Vehicles v
		INNER JOIN Makes ma ON v.MakeId = ma.MakeId
		INNER JOIN Models mo ON v.ModelId = mo.ModelId
	WHERE v.VehicleTypeId = 'N'
	GROUP BY VehicleYear, MakeName, ModelName
	ORDER BY VehicleYear DESC

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'UsedInventoryReport')
      DROP PROCEDURE UsedInventoryReport
GO

CREATE PROCEDURE UsedInventoryReport AS
BEGIN

	SELECT VehicleYear, MakeName, ModelName,
		 COUNT (*) AS CountTotal,
		 SUM (MSRP) AS StockValue
	FROM Vehicles v
		INNER JOIN Makes ma ON v.MakeId = ma.MakeId
		INNER JOIN Models mo ON v.ModelId = mo.ModelId
	WHERE v.VehicleTypeId = 'U'
	GROUP BY VehicleYear, MakeName, ModelName
	ORDER BY VehicleYear DESC

END
GO