﻿using GuildCars.Data.Interfaces;
using GuildCars.Models.Queries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;

namespace GuildCars.Data.ADO
{
    public class VehiclesRepositoryADO : IVehiclesRepository
    {

        public string searchString()
        {
            string searchItem = "SELECT TOP 20 v.VehicleId, MakeName, ModelName, BodyStyleName, TransmissionName, ExteriorColorName, InteriorName, VehicleYear, Milage, VehicleTypeName, VinNumber, Price, MSRP, ImageFileName " +
                                "FROM Vehicles v " +
                                        "INNER JOIN Makes ma ON v.MakeId = ma.MakeId " +
                                        "INNER JOIN Models mo ON v.ModelId = mo.ModelId " +
                                        "INNER JOIN BodyStyles bo ON v.BodyStyleId = bo.BodyStyleId " +
                                        "INNER JOIN Transmissions tr ON v.TransmissionId = tr.TransmissionId " +
                                        "INNER JOIN ExteriorColors ec ON v.ExteriorColorId = ec.ExteriorColorId " +
                                        "INNER JOIN Interiors ic ON v.InteriorId = ic.InteriorId " +
                                        "INNER JOIN VehicleTypes vt ON v.VehicleTypeId = vt.VehicleTypeId " +
                                        "FULL JOIN Purchases pu ON v.VehicleId = pu.VehicleId " +
                                        "FULL JOIN VehicleImages vi ON v.VehicleId = vi.VehicleId " +
                               "Where 1 = 1 AND PurchaseId IS NULL ";

            return searchItem;

        }

        public void AddVehicle(Vehicle vehicle)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("AddVehicles", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter("@VehicleId", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("@MakeId", vehicle.MakeId);
                cmd.Parameters.AddWithValue("@ModelId", vehicle.ModelId);
                cmd.Parameters.AddWithValue("@BodyStyleId", vehicle.BodyStyleId);
                cmd.Parameters.AddWithValue("@TransmissionId", vehicle.TransmissionId);
                cmd.Parameters.AddWithValue("@ExteriorColorId", vehicle.ExteriorColorId);
                cmd.Parameters.AddWithValue("@InteriorId", vehicle.InteriorId);
                cmd.Parameters.AddWithValue("@VehicleTypeId", vehicle.VehicleTypeId);
                cmd.Parameters.AddWithValue("@VehicleYear", vehicle.VehicleYear);
                cmd.Parameters.AddWithValue("@Milage", vehicle.Milage);
                cmd.Parameters.AddWithValue("@VinNumber", vehicle.VinNumber);
                cmd.Parameters.AddWithValue("@Price", vehicle.Price);
                cmd.Parameters.AddWithValue("@MSRP", vehicle.MSRP);
                cmd.Parameters.AddWithValue("@CreateDate", vehicle.CreateDate);
                cmd.Parameters.AddWithValue("ListingDescription", vehicle.ListingDescription);
                cmd.Parameters.AddWithValue("@FeatureVehicle", vehicle.FeatureVehicle);

                cn.Open();

                cmd.ExecuteNonQuery();

                vehicle.VehicleId = (int)param.Value;
            }
        }

        public void AddVehicleImage(Image image)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("AddVehicleImages", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter("@ImageId", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("@VehicleId", image.VehicleId);
                cmd.Parameters.AddWithValue("@ImageFileName", image.ImageFileName);

                cn.Open();

                cmd.ExecuteNonQuery();

                image.ImageId = (int)param.Value;
            }
        }

        public void DeleteVehicle(int vehicleId)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DeleteVehicles", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VehicleId", vehicleId);

                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        public void EditVehicle(EditVehicleItem vehicle)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("EditVehicles", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VehicleId", vehicle.VehicleId);
                cmd.Parameters.AddWithValue("@MakeId", vehicle.MakeId);
                cmd.Parameters.AddWithValue("@ModelId", vehicle.ModelId);
                cmd.Parameters.AddWithValue("@BodyStyleId", vehicle.BodyStyleId);
                cmd.Parameters.AddWithValue("@TransmissionId", vehicle.TransmissionId);
                cmd.Parameters.AddWithValue("@ExteriorColorId", vehicle.ExteriorColorId);
                cmd.Parameters.AddWithValue("@InteriorId", vehicle.InteriorId);
                cmd.Parameters.AddWithValue("@VehicleTypeId", vehicle.VehicleTypeId);
                cmd.Parameters.AddWithValue("@VehicleYear", vehicle.VehicleYear);
                cmd.Parameters.AddWithValue("@Milage", vehicle.Milage);
                cmd.Parameters.AddWithValue("@VinNumber", vehicle.VinNumber);
                cmd.Parameters.AddWithValue("@Price", vehicle.Price);
                cmd.Parameters.AddWithValue("@MSRP", vehicle.MSRP);
                cmd.Parameters.AddWithValue("ListingDescription", vehicle.ListingDescription);
                cmd.Parameters.AddWithValue("@FeatureVehicle", vehicle.FeatureVehicle);
                cmd.Parameters.AddWithValue("@ImageFileName", vehicle.ImageFileName);

                cn.Open();

                cmd.ExecuteNonQuery();
            }            
        }

        public VehicleDetailItem GetDetails(string vinNumber)
        {
            VehicleDetailItem vehicle = null;

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("VehiclesSelectDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VinNumber", vinNumber);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {      
                                 
                    if (dr.Read())
                    {
                        vehicle = new VehicleDetailItem();
                        vehicle.VehicleId = (int)dr["VehicleId"];
                        vehicle.MakeId = (int)dr["MakeId"];
                        vehicle.MakeName = dr["MakeName"].ToString();
                        vehicle.ModelId = (int)dr["ModelId"];
                        vehicle.ModelName = dr["ModelName"].ToString();
                        vehicle.BodyStyleId = (int)dr["BodyStyleId"];
                        vehicle.BodyStyleName = dr["BodyStyleName"].ToString();
                        vehicle.TransmissionId = dr["TransmissionId"].ToString();
                        vehicle.TransmissionName = dr["TransmissionName"].ToString();
                        vehicle.ExteriorColorId = (int)dr["ExteriorColorId"];
                        vehicle.ExteriorColorName = dr["ExteriorColorName"].ToString();
                        vehicle.InteriorId = (int)dr["InteriorId"];                        
                        vehicle.InteriorName = dr["InteriorName"].ToString();
                        vehicle.VehicleYear = (decimal)dr["VehicleYear"];
                        vehicle.Milage = (decimal)dr["Milage"];
                        vehicle.VinNumber = dr["VinNumber"].ToString();
                        vehicle.Price = (decimal)dr["Price"];
                        vehicle.MSRP = (decimal)dr["MSRP"];
                        vehicle.ListingDescription = dr["ListingDescription"].ToString();
                        vehicle.ImageFileName = dr["ImageFileName"].ToString();

                        if (dr["PurchaseId"] != DBNull.Value)
                        {
                            vehicle.PurchaseId = (int)dr["PurchaseId"];
                        }
                    }
                }
            }

            return vehicle;
        }

        public IEnumerable<FeaturedVehicleItem> GetFeatured()
        {
            List<FeaturedVehicleItem> featuredItem = new List<FeaturedVehicleItem>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("FeaturedVehiclesSelect", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        FeaturedVehicleItem currentRow = new FeaturedVehicleItem();
                        currentRow.VehicleId = (int)dr["VehicleId"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.VehicleYear = (decimal)dr["VehicleYear"];
                        currentRow.Price = (decimal)dr["Price"];
                        currentRow.VinNumber = dr["VinNumber"].ToString();
                        currentRow.ImageFileName = dr["ImageFileName"].ToString();

                        featuredItem.Add(currentRow);
                    }
                }
            }

            return featuredItem;
        }

        public IEnumerable<VehicleSearchResultItem> VehicleSearchAll(VehicleSearchParameters parameters)
        {
            List<VehicleSearchResultItem> SearchItem = new List<VehicleSearchResultItem>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                string query = searchString();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                if (!string.IsNullOrEmpty(parameters.QuickSearch))
                {
                    query += "AND (MakeName LIKE @QuickSearch OR ModelName LIKE @QuickSearch OR VehicleYear LIKE @QuickSearch) ";
                    cmd.Parameters.AddWithValue("@QuickSearch", parameters.QuickSearch + "%");
                }

                if (parameters.MinYear.HasValue)
                {
                    query += "AND VehicleYear >= @MinYear ";
                    cmd.Parameters.AddWithValue("@MinYear", parameters.MinYear);
                }

                if (parameters.MaxYear.HasValue)
                {
                    query += "AND VehicleYear <= @MaxYear ";
                    cmd.Parameters.AddWithValue("@MaxYear", parameters.MaxYear);
                }

                if (parameters.MinPrice.HasValue)
                {
                    query += "AND Price >= @MinPrice ";
                    cmd.Parameters.AddWithValue("@MinPrice", parameters.MinPrice);
                }

                if (parameters.MaxPrice.HasValue)
                {
                    query += "AND Price <= @MaxPrice ";
                    cmd.Parameters.AddWithValue("@MaxPrice", parameters.MaxPrice);
                }

                query += "ORDER BY MSRP DESC ";
                cmd.CommandText = query;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        VehicleSearchResultItem currentRow = new VehicleSearchResultItem();
                        currentRow.VehicleId = (int)dr["VehicleId"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.BodyStyleName = dr["BodyStyleName"].ToString();
                        currentRow.TransmissionName = dr["TransmissionName"].ToString();
                        currentRow.ExteriorColorName = dr["ExteriorColorName"].ToString();
                        currentRow.InteriorName = dr["InteriorName"].ToString();
                        currentRow.VehicleYear = (decimal)dr["VehicleYear"];
                        currentRow.Milage = (decimal)dr["Milage"];
                        currentRow.VehicleTypeName = dr["VehicleTypeName"].ToString();
                        currentRow.VinNumber = dr["VinNumber"].ToString();
                        currentRow.Price = (decimal)dr["Price"];
                        currentRow.MSRP = (decimal)dr["MSRP"];
                        currentRow.ImageFileName = dr["ImageFileName"].ToString();

                        SearchItem.Add(currentRow);
                    }
                }
            }

            return SearchItem;
        }

        public IEnumerable<VehicleSearchResultItem> VehicleSearchNew(VehicleSearchParameters parameters)
        {
            List<VehicleSearchResultItem> SearchItem = new List<VehicleSearchResultItem>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                string query = searchString() + "AND VehicleTypeName = 'New' ";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                if (!string.IsNullOrEmpty(parameters.QuickSearch))
                {
                    query += "AND (MakeName LIKE @QuickSearch OR ModelName LIKE @QuickSearch OR VehicleYear LIKE @QuickSearch) ";
                    cmd.Parameters.AddWithValue("@QuickSearch", parameters.QuickSearch + "%");
                }

                if (parameters.MinYear.HasValue)
                {
                    query += "AND VehicleYear >= @MinYear ";
                    cmd.Parameters.AddWithValue("@MinYear", parameters.MinYear);
                }

                if (parameters.MaxYear.HasValue)
                {
                    query += "AND VehicleYear <= @MaxYear ";
                    cmd.Parameters.AddWithValue("@MaxYear", parameters.MaxYear);
                }

                if (parameters.MinPrice.HasValue)
                {
                    query += "AND Price >= @MinPrice ";
                    cmd.Parameters.AddWithValue("@MinPrice", parameters.MinPrice);
                }

                if (parameters.MaxPrice.HasValue)
                {
                    query += "AND Price <= @MaxPrice ";
                    cmd.Parameters.AddWithValue("@MaxPrice", parameters.MaxPrice);
                }

                query += "ORDER BY MSRP DESC ";
                cmd.CommandText = query;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        VehicleSearchResultItem currentRow = new VehicleSearchResultItem();
                        currentRow.VehicleId = (int)dr["VehicleId"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.BodyStyleName = dr["BodyStyleName"].ToString();
                        currentRow.TransmissionName = dr["TransmissionName"].ToString();
                        currentRow.ExteriorColorName = dr["ExteriorColorName"].ToString();
                        currentRow.InteriorName = dr["InteriorName"].ToString();
                        currentRow.VehicleYear = (decimal)dr["VehicleYear"];
                        currentRow.Milage = (decimal)dr["Milage"];
                        currentRow.VehicleTypeName = dr["VehicleTypeName"].ToString();
                        currentRow.VinNumber = dr["VinNumber"].ToString();
                        currentRow.Price = (decimal)dr["Price"];
                        currentRow.MSRP = (decimal)dr["MSRP"];
                        currentRow.ImageFileName = dr["ImageFileName"].ToString();

                        SearchItem.Add(currentRow);
                    }
                }
            }

            return SearchItem;
        }

        public IEnumerable<VehicleSearchResultItem> VehicleSearchUsed(VehicleSearchParameters parameters)
        {
            List<VehicleSearchResultItem> SearchItem = new List<VehicleSearchResultItem>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                string query = searchString() + "AND VehicleTypeName = 'Used' ";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                if (!string.IsNullOrEmpty(parameters.QuickSearch))
                {
                    query += "AND (MakeName LIKE @QuickSearch OR ModelName LIKE @QuickSearch OR VehicleYear LIKE @QuickSearch) ";
                    cmd.Parameters.AddWithValue("@QuickSearch", parameters.QuickSearch + "%" );
                }

                if (parameters.MinYear.HasValue)
                {
                    query += "AND VehicleYear >= @MinYear ";
                    cmd.Parameters.AddWithValue("@MinYear", parameters.MinYear);
                }

                if (parameters.MaxYear.HasValue)
                {
                    query += "AND VehicleYear <= @MaxYear ";
                    cmd.Parameters.AddWithValue("@MaxYear", parameters.MaxYear);
                }

                if (parameters.MinPrice.HasValue)
                {
                    query += "AND Price >= @MinPrice ";
                    cmd.Parameters.AddWithValue("@MinPrice", parameters.MinPrice);
                }

                if (parameters.MaxPrice.HasValue)
                {
                    query += "AND Price <= @MaxPrice ";
                    cmd.Parameters.AddWithValue("@MaxPrice", parameters.MaxPrice);
                }

                query += "ORDER BY MSRP DESC ";
                cmd.CommandText = query;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        VehicleSearchResultItem currentRow = new VehicleSearchResultItem();
                        currentRow.VehicleId = (int)dr["VehicleId"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.BodyStyleName = dr["BodyStyleName"].ToString();
                        currentRow.TransmissionName = dr["TransmissionName"].ToString();
                        currentRow.ExteriorColorName = dr["ExteriorColorName"].ToString();
                        currentRow.InteriorName = dr["InteriorName"].ToString();
                        currentRow.VehicleYear = (decimal)dr["VehicleYear"];
                        currentRow.VehicleTypeName = dr["VehicleTypeName"].ToString();
                        currentRow.Milage = (decimal)dr["Milage"];
                        currentRow.VinNumber = dr["VinNumber"].ToString();
                        currentRow.Price = (decimal)dr["Price"];
                        currentRow.MSRP = (decimal)dr["MSRP"];
                        currentRow.ImageFileName = dr["ImageFileName"].ToString();

                        SearchItem.Add(currentRow);
                    }
                }
            }

            return SearchItem;
        }

        public EditVehicleItem GetVehicleToEdit(string vinNumber)
        {
            EditVehicleItem vehicle = new EditVehicleItem();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetVehicleToEdit", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VinNumber", vinNumber);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        vehicle.VehicleId = (int)dr["VehicleId"];
                        vehicle.MakeId = (int)dr["MakeId"];
                        vehicle.ModelId = (int)dr["ModelId"];
                        vehicle.BodyStyleId = (int)dr["BodyStyleId"];
                        vehicle.TransmissionId = dr["TransmissionId"].ToString();
                        vehicle.ExteriorColorId = (int)dr["ExteriorColorId"];
                        vehicle.InteriorId = (int)dr["InteriorId"];
                        vehicle.VehicleYear = (decimal)dr["VehicleYear"];
                        vehicle.VehicleTypeId = dr["VehicleTypeId"].ToString();
                        vehicle.VehicleYear = (decimal)dr["VehicleYear"];
                        vehicle.Milage = (decimal)dr["Milage"];
                        vehicle.VinNumber = dr["VinNumber"].ToString();
                        vehicle.Price = (decimal)dr["Price"];
                        vehicle.MSRP = (decimal)dr["MSRP"];
                        vehicle.FeatureVehicle = (bool)dr["FeatureVehicle"];
                        vehicle.ListingDescription = dr["ListingDescription"].ToString();
                        vehicle.ImageFileName = dr["ImageFileName"].ToString();
                    }
                }
            }

            return vehicle;
        }

    }
}