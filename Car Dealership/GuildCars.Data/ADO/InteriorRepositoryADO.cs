﻿using GuildCars.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;
using System.Data.SqlClient;
using System.Data;

namespace GuildCars.Data.ADO
{
    public class InteriorRepositoryADO : IInteriorsRepository
    {
        public IEnumerable<Interior> GetAll()
        {
            List<Interior> interiors = new List<Interior>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("InteriorsSelectAll", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Interior currentRow = new Interior();
                        currentRow.InteriorId = (int)dr["InteriorId"];
                        currentRow.InteriorName = dr["InteriorName"].ToString();

                        interiors.Add(currentRow);
                    }
                }
            }

            return interiors;
        }
    }
}
