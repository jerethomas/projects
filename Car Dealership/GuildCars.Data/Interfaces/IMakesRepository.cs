﻿using GuildCars.Models.Queries;
using GuildCars.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildCars.Data.Interfaces
{
    public interface IMakesRepository
    {
        IEnumerable<Make> GetAll();
        void AddMake(Make make);
        IEnumerable<MakeReportItem> GetReport();
    }
}
