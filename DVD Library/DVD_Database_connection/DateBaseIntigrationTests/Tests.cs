﻿using DVD_Database_connection.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateBaseIntigrationTests
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Init()
        {
            using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "DvdDbReset";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Connection = cn;
                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        [Test]
        public void CanLoadDvd()
        {
            //var repo = new DvdRepository();

            var dvd = DvdRepository.GetById(1);

            Assert.IsNotNull(dvd);

            Assert.AreEqual(1, dvd.DvdId);
            Assert.AreEqual("A Great Tale", dvd.Title);
            Assert.AreEqual(2015, dvd.ReleaseYear);
            Assert.AreEqual("Sam Jones", dvd.Director);
            Assert.AreEqual("PG", dvd.Rating);
            Assert.AreEqual("This really is a great tale!", dvd.Notes);

        }

        [Test]
        public void CanLoadDvdList()
        {
            //var repo = new DvdRepository();

            var dvd = DvdRepository.LoadDvdList().ToList();

            Assert.IsNotNull(dvd);

            Assert.AreEqual(1, dvd[0].DvdId);
            Assert.AreEqual("A Great Tale", dvd[0].Title);
            Assert.AreEqual(2015, dvd[0].ReleaseYear);
            Assert.AreEqual("Sam Jones", dvd[0].Director);
            Assert.AreEqual("PG", dvd[0].Rating);
            Assert.AreEqual("This really is a great tale!", dvd[0].Notes);

        }

        [Test]
        public void CanAddDvd()
        {
            DVD DvdToAdd = new DVD();
            //var repo = new DvdRepository();

            DvdToAdd.Title = "A Better Tale";
            DvdToAdd.ReleaseYear = 1989;
            DvdToAdd.Director = "Better Guy";
            DvdToAdd.Rating = "G";
            DvdToAdd.Notes = "This is a better tale!";

            DvdRepository.Insert(DvdToAdd);

            Assert.AreEqual(3, DvdToAdd.DvdId);
        }

        [Test]
        public void CanUpdateDvd()
        {
            DVD DvdToAdd = new DVD();
            var repo = new DvdRepository();

            DvdToAdd.Title = "A Better Tale";
            DvdToAdd.ReleaseYear = 1989;
            DvdToAdd.Director = "Better Guy";
            DvdToAdd.Rating = "G";
            DvdToAdd.Notes = "This is a better tale!";

            DvdRepository.Insert(DvdToAdd);

            DvdToAdd.Title = "The Best Tale";
            DvdToAdd.ReleaseYear = 1984;
            DvdToAdd.Director = "Best Guy";
            DvdToAdd.Rating = "R";
            DvdToAdd.Notes = "This is a the best tale!";

            DvdRepository.Update(DvdToAdd);

            var updateDvd = DvdRepository.GetById(3);

            Assert.AreEqual("The Best Tale", updateDvd.Title);
            Assert.AreEqual(1984, updateDvd.ReleaseYear);
            Assert.AreEqual("R", updateDvd.Rating);
            Assert.AreEqual("This is a the best tale!", updateDvd.Notes);
        }

        [Test]
        public void CanDeleteDvd()
        {
            DVD DvdToAdd = new DVD();
            var repo = new DvdRepository();

            DvdToAdd.Title = "A Better Tale";
            DvdToAdd.ReleaseYear = 1989;
            DvdToAdd.Director = "Better Guy";
            DvdToAdd.Rating = "G";
            DvdToAdd.Notes = "This is a better tale!";

            DvdRepository.Insert(DvdToAdd);

            var checkingDelete = DvdRepository.GetById(3);

            Assert.IsNotNull(checkingDelete);
            DvdRepository.Delete(3);
            checkingDelete = DvdRepository.GetById(3);

            Assert.IsNull(checkingDelete);
        }

        [Test]
        public void CanLoadDvdByTitle()
        {
            var dvd = DvdRepository.SearchByTitle("A Great Tale").ToList();

            Assert.IsNotNull(dvd);

            Assert.AreEqual(1, dvd[0].DvdId);
            Assert.AreEqual("A Great Tale", dvd[0].Title);
            Assert.AreEqual(2015, dvd[0].ReleaseYear);
            Assert.AreEqual("Sam Jones", dvd[0].Director);
            Assert.AreEqual("PG", dvd[0].Rating);
            Assert.AreEqual("This really is a great tale!", dvd[0].Notes);

        }

        [Test]
        public void CanLoadDvdByReleaseYear()
        {
            var dvd = DvdRepository.SearchByReleaseYear(2015).ToList();

            Assert.IsNotNull(dvd);

            Assert.AreEqual(1, dvd[0].DvdId);
            Assert.AreEqual("A Great Tale", dvd[0].Title);
            Assert.AreEqual(2015, dvd[0].ReleaseYear);
            Assert.AreEqual("Sam Jones", dvd[0].Director);
            Assert.AreEqual("PG", dvd[0].Rating);
            Assert.AreEqual("This really is a great tale!", dvd[0].Notes);

        }

        [Test]
        public void CanLoadDvdByDirector()
        {
            var dvd = DvdRepository.SearchByDirector("Sam Jones").ToList();

            Assert.IsNotNull(dvd);

            Assert.AreEqual(1, dvd[0].DvdId);
            Assert.AreEqual("A Great Tale", dvd[0].Title);
            Assert.AreEqual(2015, dvd[0].ReleaseYear);
            Assert.AreEqual("Sam Jones", dvd[0].Director);
            Assert.AreEqual("PG", dvd[0].Rating);
            Assert.AreEqual("This really is a great tale!", dvd[0].Notes);

        }

        [Test]
        public void CanLoadDvdByRating()
        {
            var dvd = DvdRepository.SearchByRating("PG").ToList();

            Assert.IsNotNull(dvd);

            Assert.AreEqual(1, dvd[0].DvdId);
            Assert.AreEqual("A Great Tale", dvd[0].Title);
            Assert.AreEqual(2015, dvd[0].ReleaseYear);
            Assert.AreEqual("Sam Jones", dvd[0].Director);
            Assert.AreEqual("PG", dvd[0].Rating);
            Assert.AreEqual("This really is a great tale!", dvd[0].Notes);

        }
    }
}
