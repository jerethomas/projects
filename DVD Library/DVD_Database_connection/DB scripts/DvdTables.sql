USE Dvd
GO

IF EXISTS(SELECT * FROM sys.tables WHERE name='DvdDetails')
	DROP TABLE DvdDetails
GO

CREATE TABLE DvdDetails (
	DvdId int identity(1,1) not null primary key,
	Title nvarchar(50) not null,
	ReleaseYear int not null,
	Director nvarchar(50) not null, 
	Rating nvarchar (5) not null,
	Notes nvarchar(100) null
)