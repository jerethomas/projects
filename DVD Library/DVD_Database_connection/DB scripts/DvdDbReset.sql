USE Dvd
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'DvdDbReset')
      DROP PROCEDURE DvdDbReset
GO

CREATE PROCEDURE DvdDbReset AS
BEGIN
	DELETE FROM DvdDetails;

	DBCC CHECKIDENT ('DvdDetails', RESEED, 1)

	SET IDENTITY_INSERT DvdDetails ON;

	INSERT INTO DvdDetails (DvdId, Title, ReleaseYear, Director, Rating, Notes)
	VALUES (1, 'A Great Tale', '2015', 'Sam Jones', 'PG', 'This really is a great tale!'),
	(2, 'A Good Tale', '2012', 'Jow Smith', 'PG-13', 'This is a good tale!')

	SET IDENTITY_INSERT DvdDetails OFF;
END