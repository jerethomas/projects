﻿using DVD_Database_connection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DVD_Database_connection.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DvdController : ApiController
    {
        [Route("dvds")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetAllDVd()
        {
            return Ok(DvdRepository.LoadDvdList());
        }

        [Route("dvds/title/{title}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetDVdByTitle(string title)
        {
            List<DVD> dvd = DvdRepository.SearchByTitle(title).ToList();

            if (dvd == null)
            {
                return NotFound();
            }

            return Ok(dvd);
        }

        [Route("dvds/releaseYear/{releaseYear}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetDVdByReleaseYear(int releaseYear)
        {
            List<DVD> dvd = DvdRepository.SearchByReleaseYear(releaseYear).ToList();

            if (dvd == null)
            {
                return NotFound();
            }

            return Ok(dvd);
        }

        [Route("dvds/director/{director}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetDVdByDirectore(string director)
        {
            List<DVD> dvd = DvdRepository.SearchByDirector(director).ToList();

            if (dvd == null)
            {
                return NotFound();
            }

            return Ok(dvd);
        }

        [Route("dvds/rating/{rating}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetDVdByRating(string rating)
        {
            List<DVD> dvd = DvdRepository.SearchByRating(rating).ToList();

            if (dvd == null)
            {
                return NotFound();
            }

            return Ok(dvd);
        }

        [Route("dvd/{id}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult Get(int id)
        {
            DVD dvd = DvdRepository.GetById(id);

            if(dvd == null)
            {
                return NotFound();
            }

            return Ok(dvd);
        }

        [Route("dvd")]
        [AcceptVerbs("POST")]
        public IHttpActionResult Add(DVD dvd)
        {
            DvdRepository.Insert(dvd);

            return Created($"contact/{dvd.DvdId}", dvd);
        }

        [Route("dvd/{id}")]
        [AcceptVerbs("PUT")]
        public void Update(int id, DVD dvd)
        {
            DvdRepository.Update(dvd);
        }

        [Route("dvd/{id}")]
        [AcceptVerbs("DELETE")]
        public void Delete(int id)
        {
            DvdRepository.Delete(id);
        }
    }
}
