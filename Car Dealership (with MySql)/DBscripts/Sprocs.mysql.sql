Use GuildCars;


DELIMITER $$

DROP PROCEDURE IF EXISTS MakesSelectAll; $$

CREATE PROCEDURE MakesSelectAll ()
BEGIN
	SELECT MakeId, MakeName, CreatedDate, UserId
	FROM Makes;
END $$
    


DROP PROCEDURE IF EXISTS MakesReport; $$

CREATE PROCEDURE MakesReport ()
BEGIN
	SELECT MakeId, MakeName, CreatedDate, Email		
	FROM Makes ma
		INNER JOIN AspNetUsers aspu ON ma.UserId = aspu.Id;
END $$



DROP PROCEDURE IF EXISTS ModelsReport; $$

CREATE PROCEDURE ModelsReport ()
BEGIN
	SELECT ModelId, ModelName, MakeName, mo.CreatedDate, Email		
	FROM Models mo
		INNER JOIN Makes ma ON mo.MakeId = ma.MakeId
		INNER JOIN AspNetUsers aspu ON mo.UserId = aspu.Id
	ORDER BY MakeName;
END $$



DROP PROCEDURE IF EXISTS ModelsSelectAll; $$

CREATE PROCEDURE ModelsSelectAll ()
BEGIN	
    SELECT ModelId, MakeId, ModelName, CreatedDate, UserId
	FROM Models;
END $$



DROP PROCEDURE IF EXISTS BodyStylesSelectAll; $$

CREATE PROCEDURE BodyStylesSelectAll ()
BEGIN
	SELECT BodyStyleId, BodyStyleName
	FROM BodyStyles;
END $$



DROP PROCEDURE IF EXISTS TransmissionsSelectAll; $$

CREATE PROCEDURE TransmissionsSelectAll ()
BEGIN
	SELECT TransmissionId, TransmissionName
	FROM Transmissions;
END $$



DROP PROCEDURE IF EXISTS ExteriorColorsSelectAll; $$

CREATE PROCEDURE ExteriorColorsSelectAll ()
BEGIN	
    SELECT ExteriorColorId, ExteriorColorName
	FROM ExteriorColors;
END $$



DROP PROCEDURE IF EXISTS VehicleTypesSelectAll; $$

CREATE PROCEDURE VehicleTypesSelectAll ()
BEGIN
	SELECT VehicleTypeId, VehicleTypeName
	FROM VehicleTypes;
END $$



DROP PROCEDURE IF EXISTS InteriorsSelectAll; $$

CREATE PROCEDURE InteriorsSelectAll ()
BEGIN
	SELECT InteriorId, InteriorName
	FROM Interiors;
END $$



DROP PROCEDURE IF EXISTS PurchasesSelectAll; $$

CREATE PROCEDURE PurchasesSelectAll ()
BEGIN
	SELECT PurchaseId, VehicleId, PurchaseName, Phone, Email, Street1, Street2, City, StateId, Zipcode, PurchasePrice, PurchaseTypeId, PurchaseDate, UserId
	FROM Purchases;
END $$



DROP PROCEDURE IF EXISTS PurchaseTypesSelectAll; $$

CREATE PROCEDURE PurchaseTypesSelectAll ()
BEGIN
	SELECT PurchaseTypeId, PurchaseTypeName
	FROM PurchaseTypes;
END $$



DROP PROCEDURE IF EXISTS StatesSelectAll; $$

CREATE PROCEDURE StatesSelectAll ()
BEGIN
	SELECT StateId, StateName
	FROM States;
END $$



DROP PROCEDURE IF EXISTS SpecialsSelectAll; $$


CREATE PROCEDURE SpecialsSelectAll ()
BEGIN
	SELECT SpecialId, SpecialName, SpecialDescription, SepcialImageFileName
	FROM Specials;
END $$



DROP PROCEDURE IF EXISTS FeaturedVehiclesSelect; $$

CREATE PROCEDURE FeaturedVehiclesSelect ()
BEGIN
	SELECT v.VehicleId, MakeName, ModelName, VehicleYear, Price, ImageFileName, VinNumber, FeatureVehicle
	FROM Vehicles v
		LEFT JOIN Makes ma ON v.MakeId = ma.MakeId
		LEFT JOIN Models mo ON v.ModelId = mo.ModelId
		LEFT JOIN VehicleImages vi ON v.VehicleId = vi.VehicleId
		LEFT JOIN Purchases pu ON v.VehicleId = pu.VehicleId
	WHERE FeatureVehicle = 1 AND PurchaseId IS NULL
    LIMIT 20;
END $$



DROP PROCEDURE IF EXISTS GetModelByMake; $$

CREATE PROCEDURE GetModelByMake (
	IN MakeIdIN int
)
BEGIN 
	SELECT ModelId, ModelName
	FROM Models mo
		INNER JOIN Makes ma ON mo.MakeId = ma.MakeId
	WHERE mo.MakeId = MakeIdIN;
END $$



DROP PROCEDURE IF EXISTS VehiclesSelectDetails; $$

CREATE PROCEDURE VehiclesSelectDetails (
	IN VinNumberIN varchar(17)
)
BEGIN
	SELECT  v.VehicleId, ma.MakeId, MakeName, mo.ModelId, ModelName, bo.BodyStyleId, BodyStyleName, 
	v.TransmissionId, TransmissionName, v.ExteriorColorId, ExteriorColorName, v.InteriorId, InteriorName, 
	VehicleYear, Milage, VinNumber, Price, MSRP, ListingDescription, PurchaseId, ImageFileName 
	FROM Vehicles v
		LEFT JOIN Makes ma ON v.MakeId = ma.MakeId
		LEFT JOIN Models mo ON v.ModelId = mo.ModelId
		LEFT JOIN BodyStyles bo ON v.BodyStyleId = bo.BodyStyleId
		LEFT JOIN Transmissions tr ON v.TransmissionId = tr.TransmissionId
		LEFT JOIN ExteriorColors ec ON v.ExteriorColorId = ec.ExteriorColorId
		LEFT JOIN Interiors ic ON v.InteriorId = ic.InteriorId
		LEFT OUTER JOIN VehicleImages vi ON v.VehicleId = vi.VehicleId
		LEFT OUTER JOIN Purchases pu ON v.VehicleId = pu.VehicleId
	WHERE VinNumber = VinNumberIN;
END $$
    


DROP PROCEDURE IF EXISTS GetVehicleToEdit; $$

CREATE PROCEDURE GetVehicleToEdit (
	IN VinNumberIN varchar(17)
)
BEGIN
	SELECT  v.VehicleId, v.MakeId, v.ModelId, v.BodyStyleId, v.TransmissionId, v.ExteriorColorId, v.InteriorId, VehicleTypeId,
	VehicleYear, Milage, VinNumber, Price, MSRP, ListingDescription, ImageFileName, FeatureVehicle
	FROM Vehicles v		
		LEFT JOIN VehicleImages vi ON v.VehicleId = vi.VehicleId
	WHERE VinNumber = VinNumberIN;
END $$



DROP PROCEDURE IF EXISTS AddMakes; $$

CREATE PROCEDURE AddMakes (	
	IN MakeNameIN varchar(25),
	IN CreatedDateIN datetime,
	IN UserIdIN nvarchar(128),
    OUT MakeIdOUT int
)
BEGIN
	INSERT INTO Makes (MakeName, CreatedDate, UserId)
	VALUES (MakeNameIN, CreatedDateIN, UserIdIN);

	SET MakeIdOUT = LAST_INSERT_ID();
END $$



DROP PROCEDURE IF EXISTS AddModels; $$

CREATE PROCEDURE AddModels (
	OUT ModelIdOUT int,
	IN MakeIdIN int,
	IN ModelNameIN varchar(25),
	IN CreatedDateIN datetime,
	IN UserIdIN nvarchar(128)
)
BEGIN
	INSERT INTO Models(MakeId, ModelName, CreatedDate, UserId)
	VALUES (MakeIdIN, ModelNameIN, CreatedDateIN, UserIdIN);

	SET ModelIdOUT = LAST_INSERT_ID();
END $$



DROP PROCEDURE IF EXISTS AddSpecials; $$

CREATE PROCEDURE AddSpecials (
	OUT SpecialIdOUT int,
	IN SpecialNameIN varchar(50),
	IN SpecialDescriptionIN varchar(500),
	IN SepcialImageFileNameIN varchar(50)
)
BEGIN
	INSERT INTO Specials (SpecialName, SpecialDescription, SepcialImageFileName)
	VALUES (SpecialNameIN, SpecialDescriptionIN, SepcialImageFileNameIN);

	SET SpecialIdOUT = LAST_INSERT_ID();
END $$



DROP PROCEDURE IF EXISTS AddContacts; $$

CREATE PROCEDURE AddContacts (
	OUT ContactIdOUT int,
	IN ContactNameIN varchar(50),
	IN ContactEmailIN varchar(50),
	IN ContactPhoneIN varchar(18),
	IN ContactMessageIN varchar(500)
)
BEGIN
	INSERT INTO Contacts (ContactName, ContactEmail, ContactPhone, ContactMessage)
	VALUES (ContactNameIN, ContactEmailIN, ContactPhoneIN, ContactMessageIN);

	SET ContactIdOUT = LAST_INSERT_ID();
END $$



DROP PROCEDURE IF EXISTS AddPurchase; $$

CREATE PROCEDURE AddPurchase (
	OUT PurchaseIdOUT int,
	IN VehicleIdIN int,
	IN PurchaseNameIN varchar(50),
	IN PhoneIN varchar(18),
	IN EmailIN varchar(50),
	IN Street1IN varchar(50),
	IN Street2IN varchar(50),
	IN CityIN varchar(50),
	IN StateIdIN varchar(2),
	IN ZipcodeIN decimal(5),
	IN PurchasePriceIN decimal(8,2),
	IN PurchaseTypeIdIN char(1),
	IN PurchaseDateIN datetime,
	IN UserIdIN nvarchar(128)
)
BEGIN
	INSERT INTO Purchases (VehicleId, PurchaseName, Phone, Email, Street1, Street2, City, StateId, Zipcode, PurchasePrice, PurchaseTypeId, PurchaseDate, UserId)
	VALUES (VehicleIdIN, PurchaseNameIN, PhoneIN, EmailIN, Street1IN, Street2IN, CityIN, StateIdIN, ZipcodeIN, PurchasePriceIN, PurchaseTypeIdIN, PurchaseDateIN, UserIdIN);

	SET PurchaseIdOUT = LAST_INSERT_ID();
END $$



DROP PROCEDURE IF EXISTS AddVehicleImages; $$

CREATE PROCEDURE AddVehicleImages (
	OUT ImageIdOUT int ,
	IN VehicleIdIN int,
	IN ImageFileNameIN varchar(50)

)
BEGIN
	INSERT INTO VehicleImages (VehicleId, ImageFileName)
	VALUES (VehicleIdIN, ImageFileNameIN);

	SET ImageIdOUT = LAST_INSERT_ID();
END $$



DROP PROCEDURE IF EXISTS AddVehicles; $$

CREATE PROCEDURE AddVehicles (
	OUT VehicleIdOUT int,
	IN MakeIdIN int,
	IN ModelIdIN int,
	IN BodyStyleIdIN int ,
	IN TransmissionIdIN char(1),
	IN ExteriorColorIdIN int,
	IN InteriorIdIN int,
	IN VehicleTypeIdIN char(1),
	IN VehicleYearIN decimal(4),
	IN MilageIN decimal(9,2),
	IN VinNumberIN varchar(17),
	IN PriceIN decimal(8,2),
	IN MSRPIN decimal(8,2),
	IN CreateDateIN datetime,
	IN ListingDescriptionIN varchar(500),
	IN FeatureVehicleIN bit
)
BEGIN
	INSERT INTO Vehicles (MakeId, ModelId, BodyStyleId, TransmissionId, ExteriorColorId, InteriorId, 
	VehicleTypeId, VehicleYear, Milage, VinNumber, Price, MSRP, CreateDate, ListingDescription, FeatureVehicle)
	VALUES (MakeIdIN, ModelIdIN, BodyStyleIdIN, TransmissionIdIN, ExteriorColorIdIN, InteriorIdIN, 
	VehicleTypeIdIN, VehicleYearIN, MilageIN, VinNumberIN, PriceIN, MSRPIN, CreateDateIN, ListingDescriptionIN, FeatureVehicleIN);

	SET VehicleIdOUT = LAST_INSERT_ID();
END $$



DROP PROCEDURE IF EXISTS EditVehicles; $$

CREATE PROCEDURE EditVehicles (
	IN VehicleIdIN int,
	IN MakeIdIN int,
	IN ModelIdIN int,
	IN BodyStyleIdIN int ,
	IN TransmissionIdIN char(1),
	IN ExteriorColorIdIN int,
	IN InteriorIdIN int,
	IN VehicleTypeIdIN char(1),
	IN VehicleYearIN decimal(4),
	IN MilageIN decimal(9,2),
	IN VinNumberIN varchar(17),
	IN PriceIN decimal(8,2),
	IN MSRPIN decimal(8,2),
	IN ListingDescriptionIN varchar(500),
	IN FeatureVehicleIN bit,
	IN ImageFileNameIN varchar(50)
)
BEGIN
	UPDATE Vehicles SET 
	MakeId = MakeIdIN,
	ModelId = ModelIdIN,
	BodyStyleId = BodyStyleIdIN,
	TransmissionId = TransmissionIdIN, 
	ExteriorColorId = ExteriorColorIdIN, 
	InteriorId = InteriorIdIN, 
	VehicleTypeId = VehicleTypeIdIN, 
	VehicleYear = VehicleYearIN, 
	Milage = MilageIN, 
	VinNumber = VinNumberIN, 
	Price = PriceIN, 
	MSRP = MSRPIN,
	ListingDescription = ListingDescriptionIN, 
	FeatureVehicle = FeatureVehicleIN

	WHERE VehicleId = VehicleIdIN;

	UPDATE VehicleImages SET
	ImageFileName = ImageFileNameIN
	WHERE VehicleId = VehicleIdIN;
END $$



DROP PROCEDURE IF EXISTS DeleteSpecials; $$

CREATE PROCEDURE DeleteSpecials (
	IN SpecialIdIN int
)
BEGIN
	START TRANSACTION;

	DELETE FROM Specials 
	WHERE SpecialId = SpecialIdIN;

	COMMIT;
END $$



DROP PROCEDURE IF EXISTS DeleteVehicles; $$

CREATE PROCEDURE DeleteVehicles (
	IN VehicleIdIN int
) 
BEGIN
	START TRANSACTION;

	DELETE FROM VehicleImages WHERE VehicleId = VehicleIdIN;
	DELETE FROM Vehicles WHERE VehicleId = VehicleIdIN;

	COMMIT;
END $$



DROP PROCEDURE IF EXISTS NewInventoryReport; $$

CREATE PROCEDURE NewInventoryReport ()
BEGIN
	SELECT VehicleYear, MakeName, ModelName,
		 COUNT(*) AS CountTotal,
		 SUM(MSRP) AS StockValue
	FROM Vehicles v
		INNER JOIN Makes ma ON v.MakeId = ma.MakeId
		INNER JOIN Models mo ON v.ModelId = mo.ModelId
	WHERE v.VehicleTypeId = 'N'
	GROUP BY VehicleYear, MakeName, ModelName
	ORDER BY VehicleYear DESC;
END $$



DROP PROCEDURE IF EXISTS UsedInventoryReport; $$

CREATE PROCEDURE UsedInventoryReport ()
BEGIN

	SELECT VehicleYear, MakeName, ModelName,
		 COUNT(*) AS CountTotal,
		 SUM(MSRP) AS StockValue
	FROM Vehicles v
		INNER JOIN Makes ma ON v.MakeId = ma.MakeId
		INNER JOIN Models mo ON v.ModelId = mo.ModelId
	WHERE v.VehicleTypeId = 'U'
	GROUP BY VehicleYear, MakeName, ModelName
	ORDER BY VehicleYear DESC;
END $$