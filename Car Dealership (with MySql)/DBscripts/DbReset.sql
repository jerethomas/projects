Use GuildCars
GO

IF EXISTS(SELECT *FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME = 'DbReset')
		DROP PROCEDURE DbReset
GO

CREATE PROCEDURE DbReset AS
BEGIN
	DELETE FROM Specials;
	DELETE FROM Contacts;
	DELETE FROM Purchases;
	DELETE FROM VehicleImages;
	DELETE FROM Vehicles;
	DELETE FROM VehicleTypes;
	DELETE FROM PurchaseTypes;	
	DELETE FROM States;
	DELETE FROM Models;
	DELETE FROM Makes;
	DELETE FROM BodyStyles;
	DELETE FROM Transmissions;
	DELETE FROM ExteriorColors;
	DELETE FROM Interiors;	
	DELETE FROM AspNetUsers WHERE id IN ('00000000-1111-1111-1111-111111111111', '22222222-1111-1111-1111-111111111111', '11111111-1111-1111-1111-111111111111');
	DELETE FROM AspNetUserRoles WHERE UserId IN ('00000000-1111-1111-1111-111111111111');

	DBCC CHECKIDENT ('Vehicles', RESEED, 1)
	DBCC CHECKIDENT ('Makes', RESEED, 1)
	DBCC CHECKIDENT ('Models', RESEED, 1)
	DBCC CHECKIDENT ('Specials', RESEED, 1)	
	DBCC CHECKIDENT ('Contacts', RESEED, 1)	
	DBCC CHECKIDENT ('Purchases', RESEED, 1)	
	DBCC CHECKIDENT ('VehicleImages', RESEED, 1)	
	
	INSERT INTO AspNetUsers(Id, EmailConfirmed, PhoneNumberConfirmed, Email, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, UserName, FirstName, LastName)
	VALUES('11111111-1111-1111-1111-111111111111', 0, 0, 'user1@test.com', 0, 0, 0, 'user1', 'User1', 'Test1'),
	('22222222-1111-1111-1111-111111111111', 0, 0, 'user2@test.com', 0, 0, 0, 'user2', 'User2', 'Test2'),
	('00000000-1111-1111-1111-111111111111', 0, 0, 'salesuser1@test.com', 0, 0, 0, 'Sales User','User3', 'Test3')

	INSERT INTO AspNetUserRoles (UserId, RoleId)
	VALUES('00000000-1111-1111-1111-111111111111','df1fdbf6-985b-403f-b04d-26f9a25d3d5b')
	
	SET IDENTITY_INSERT Makes ON;
	INSERT INTO Makes (MakeId, MakeName, CreatedDate, UserId)
	VALUES(1, 'Acura', GETUTCDATE(), '11111111-1111-1111-1111-111111111111'),
	(2, 'Audi', GETUTCDATE(), '22222222-1111-1111-1111-111111111111'),
	(3, 'Bugatti', GETUTCDATE(), '11111111-1111-1111-1111-111111111111'),
	(4, 'Buick', GETUTCDATE(), '22222222-1111-1111-1111-111111111111'),
	(5, 'Dodge', GETUTCDATE(), '22222222-1111-1111-1111-111111111111'),
	(6, 'Ford', GETUTCDATE(), '11111111-1111-1111-1111-111111111111')

	SET IDENTITY_INSERT Makes OFF;

	SET IDENTITY_INSERT Models ON;
	INSERT INTO Models (MakeId, ModelId, ModelName, CreatedDate, UserId)
	VALUES(2, 1, 'A4', GETUTCDATE(), '11111111-1111-1111-1111-111111111111'),
	(1, 2, 'MDX', GETUTCDATE(), '11111111-1111-1111-1111-111111111111'),
	(5, 3, 'Ram', GETUTCDATE(), '22222222-1111-1111-1111-111111111111'),
	(2, 4, 'A7', GETUTCDATE(), '11111111-1111-1111-1111-111111111111'),
	(3, 5, 'Veyron', GETUTCDATE(), '11111111-1111-1111-1111-111111111111'),
	(4, 6, 'Regal', GETUTCDATE(), '22222222-1111-1111-1111-111111111111'),
	(5, 7, 'Charger', GETUTCDATE(), '11111111-1111-1111-1111-111111111111'),
	(6, 8, 'Mustang', GETUTCDATE(), '22222222-1111-1111-1111-111111111111'),
	(6, 9, 'Edge', GETUTCDATE(), '11111111-1111-1111-1111-111111111111')

	SET IDENTITY_INSERT Models OFF;
	
	INSERT INTO VehicleTypes (VehicleTypeId, VehicleTypeName)
	VALUES('N', 'New'),
	('U', 'Used')

	SET IDENTITY_INSERT BodyStyles ON;
	INSERT INTO BodyStyles (BodyStyleId, BodyStyleName)
	VALUES(1, 'Car'),
	(2, 'SUV'),
	(3, 'Truck'),
	(4, 'Van')

	SET IDENTITY_INSERT BodyStyles OFF;
	
	INSERT INTO Transmissions (TransmissionId, TransmissionName)
	VALUES('A', 'Automatic'),
	('M', 'Manual')
	
	SET IDENTITY_INSERT ExteriorColors ON;
	INSERT INTO ExteriorColors (ExteriorColorId, ExteriorColorName)
	VALUES(1, 'Red'),
	(2, 'Blue'),
	(3, 'Green'),
	(4, 'White'),
	(5, 'Black')

	SET IDENTITY_INSERT ExteriorColors OFF;
	
	SET IDENTITY_INSERT Interiors ON;
	INSERT INTO Interiors (InteriorId, InteriorName)
	VALUES(1, 'Cream'),
	(2, 'Dark Blue'),
	(3, 'Black Leather'),
	(4, 'Cream Leather'),
	(5, 'Brown Leather')

	SET IDENTITY_INSERT Interiors OFF;

	INSERT INTO States (StateId, StateName)
	VALUES ('OH', 'Ohio'),
	('KY', 'Kentucky'),
	('MN', 'Minnesota'),
	('FL', 'Florida')

	INSERT INTO PurchaseTypes (PurchaseTypeId, PurchaseTypeName)
	VALUES ('B', 'Bank Finance'),
	('C', 'Cash'),
	('D', 'Dealer Finance')

	SET IDENTITY_INSERT Vehicles ON;
	INSERT INTO Vehicles (VehicleId, MakeId, ModelId, BodyStyleId, TransmissionId, ExteriorColorId, InteriorId, VehicleTypeId, VehicleYear, Milage, VinNumber, Price, MSRP, CreateDate, ListingDescription, FeatureVehicle)
	VALUES(1, 3, 5, 1, 'A', 5, 3, 'N', 2018, 10.01, '123asd987qwe14789', 180000.99, 200000.99, GETUTCDATE(), 'Fast', 1),
	(2, 6, 9, 2, 'M', 1, 1, 'U', 2001, 17002.65, '313dfg741poi6547d', 65000.97, 65000.99, GETUTCDATE(), 'Fits everyone', 1),
	(3, 5, 7, 1, 'A', 2, 4, 'N', 2017, 500.21, '345cvb654cbb32165', 24500.99, 50000.99, GETUTCDATE(), 'Pretty Fast', 1),
	(4, 4, 6, 1, 'M', 4, 2, 'U', 2005, 35000.89, '253daev87qwe65498', 80000.99, 80000.99, GETUTCDATE(), 'Fast and Regal', 1),
	(5, 2, 1, 1, 'A', 1, 5, 'N', 2017, 500.89, '8pbg30rmnfbzd74q1', 29999.99, 30000.99, GETUTCDATE(), 'Fast and cheap', 1),
	(6, 1, 2, 2, 'A', 4, 4, 'U', 2011, 34500.89, 'moavzrx5453xngeb5', 49999.99, 50000.99, GETUTCDATE(), 'Fast and fits everyone', 1),
	(7, 4, 6, 1, 'M', 1, 2, 'N', 2017, 500.89, 'p4xvhjmm5dzqsf13q', 90000.99, 90000.99, GETUTCDATE(), 'Fast and Regal 2', 1),
	(8, 6, 8, 1, 'A', 4, 3, 'U', 2008, 43500.89, 'dwsneleb4pegrmtd2', 30000.99, 30000.99, GETUTCDATE(), 'Fix or repair daily', 0),
	(9, 3, 5, 1, 'A', 5, 3, 'N', 2017, 500.89, '4ialsid9sgccc9j9c', 40000.99, 40000.99, GETUTCDATE(), 'Fast 2', 1),
	(10, 1, 3, 1, 'M', 4, 1, 'U', 2006, 32500.89, '396rtg35r8vsni6df', 49999.99, 50000.99, GETUTCDATE(), 'Fast', 1),
	(11, 5, 3, 3, 'A', 5, 3, 'U', 2008, 21500.89, 'p4xvhjmm5dzqsf12q', 10000.99, 10000.99, GETUTCDATE(), 'its a ram', 0),
	(12, 2, 4, 1, 'A', 5, 4, 'N', 2016, 500.89, 'p4xvhj125dzqsf68q', 20000.99, 20000.99, GETUTCDATE(), 'its an audi', 1),
	(13, 3, 5, 1, 'A', 5, 4, 'N', 2018, 500.89, 'p4xvhjas5dzqsf68q', 20000.99, 20000.99, GETUTCDATE(), 'Fast2', 1),
	(14, 3, 5, 1, 'A', 5, 4, 'N', 2018, 500.89, 'p4xvhjm35dzqsf68q', 20000.99, 20000.99, GETUTCDATE(), 'Fast3', 1),
	(15, 6, 9, 1, 'A', 5, 4, 'U', 2001, 500.89, 'p4xvhjmfd5dzqsf68', 20000.99, 20000.99, GETUTCDATE(), 'Fits everyone 2', 1)

	SET IDENTITY_INSERT Vehicles OFF;

	SET IDENTITY_INSERT VehicleImages ON;
	INSERT INTO VehicleImages(ImageId, VehicleId, ImageFileName)
	VALUES (1, 1, 'bugatiiveyron.png' ),
	(2, 2, 'fordedge.png'),
	(3, 3, 'DodgeCharger.png'),
	(4, 4, 'BuickRegal.png'),
	(5, 5, 'AcuraA4.png'),
	(6, 6, 'AcuraMDX.png'),
	(7, 7, 'BuickRegal2.png'),
	(8, 8, 'FordMustang.png'),
	(9, 9, 'bugatiiveyron2.png'),
	(10, 10, 'acuratlx.png'),
	(11, 11, 'dodgeram.png'),
	(12, 12, 'audia7.png'),
	(13, 13, 'bugatiiveyron.png'),
	(14, 14, 'bugatiiveyron.png'),
	(15, 15, 'fordedge.png')

	SET IDENTITY_INSERT VehicleImages OFF;

	SET IDENTITY_INSERT Purchases ON;
	INSERT INTO Purchases (PurchaseId, VehicleId, PurchaseName, Phone, Email, Street1, Street2, City, StateId, Zipcode, PurchasePrice, PurchaseTypeId, PurchaseDate, UserId)
	VALUES (1, 3, 'Eric W.', '0321654987', 'EricW@guild.com', '789 Somewhere Ave', null, 'Cincinnati', 'OH', 65478, 150000.01, 'C', '2017-01-10', '00000000-1111-1111-1111-111111111111'),
	(2, 4, 'Testy Mc.Testerson', '6549871230', null, '504 Surgar Lane', null, 'Ponyville', 'FL', 98745, 987654.12, 'B', '2017-02-10', '11111111-1111-1111-1111-111111111111'),
	(3, 5, 'Dick Butt', null, 'dickbutt@HQG.com', 'HQG Subbreddit Ave', null, 'Reddit', 'MN', 65412, 200.12, 'D', '2017-02-20', '20e82233-d2e8-4953-9868-94907fdc70c7'),
	(4, 10, 'Dick Butt', null, 'dickbutt@HQG.com', 'HQG Subbreddit Ave', null, 'Reddit', 'MN', 65412, 200.12, 'D', '2017-03-10', '00000000-1111-1111-1111-111111111111'),
	(5, 11, 'Dick Butt', null, 'dickbutt@HQG.com', 'HQG Subbreddit Ave', null, 'Reddit', 'MN', 65412, 200.12, 'D', '2017-03-20', '20e82233-d2e8-4953-9868-94907fdc70c7'),
	(6, 15, 'Dick Butt', null, 'dickbutt@HQG.com', 'HQG Subbreddit Ave', null, 'Reddit', 'MN', 65412, 200.12, 'D', '2017-03-30', '00000000-1111-1111-1111-111111111111')

	SET IDENTITY_INSERT Purchases OFF;

	SET IDENTITY_INSERT Specials ON;
	INSERT INTO Specials (SpecialId, SpecialName, SpecialDescription, SepcialImageFileName)
	VALUES(1, '$1 Down!', '$1 Gets you a slightly used car!', '1dollar.png'),
	(2, '$2 Down!', '$2''s Gets you a used car!', '2dollar.png'),
	(3, '$3 Down!', '$3''s Gets you a really used car. Hey, its a car!', '3dollar.png')

	SET IDENTITY_INSERT Specials OFF;

	SET IDENTITY_INSERT Contacts ON;
	INSERT INTO Contacts (ContactId, ContactName, ContactEmail, ContactPhone, ContactMessage)
	VALUES(1, 'Jeff B.', 'JeffB@bjeff.com', '1234567890', 'My kid puked in my car so I need a new one.'),
	(2, 'Meth Jeff', 'MethJ@bjeff.com', '0987654321', 'My kid puked in my husband''s car so I want to get him new one.'),
	(3, 'B. Rouse', 'test@test.com', '6549873210', 'rawr message')

	SET IDENTITY_INSERT Contacts OFF;
END