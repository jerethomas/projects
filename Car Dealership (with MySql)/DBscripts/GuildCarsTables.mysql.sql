Use GuildCars;


DROP TABLE IF EXISTS Specials;

DROP TABLE IF EXISTS Contacts;

DROP TABLE IF EXISTS Purchases;

DROP TABLE IF EXISTS VehicleImages;

DROP TABLE IF EXISTS Vehicles;

DROP TABLE IF EXISTS VehicleTypes;

DROP TABLE IF EXISTS PurchaseTypes;

DROP TABLE IF EXISTS States;

DROP TABLE IF EXISTS Models;

DROP TABLE IF EXISTS Makes;

DROP TABLE IF EXISTS BodyStyles;

DROP TABLE IF EXISTS Transmissions;

DROP TABLE IF EXISTS ExteriorColors;

DROP TABLE IF EXISTS Interiors;

CREATE TABLE Makes (
	MakeId int not null AUTO_INCREMENT,
	MakeName varchar(25) not null,
	CreatedDate datetime not null,
	UserId nvarchar(128) not null,
    primary key (MakeId),
    foreign key (UserId) references AspNetUsers(Id)
);

CREATE TABLE Models (
	ModelId int not null AUTO_INCREMENT,
	MakeId int not null,
	ModelName varchar(25) not null,
	CreatedDate datetime not null,
	UserId nvarchar(128)not null,
    
    primary key (ModelId),
    foreign key(MakeId) references Makes(MakeId),
    foreign key(UserId) references AspNetUsers(Id)
);

CREATE TABLE BodyStyles (
	BodyStyleId int not null AUTO_INCREMENT,
	BodyStyleName varchar(25) not null,
    primary key (BodyStyleId)
);

CREATE TABLE Transmissions (
	TransmissionId char(1) not null,
	TransmissionName varchar(25) not null,
    primary key (TransmissionId)
);

CREATE TABLE ExteriorColors (
	ExteriorColorId int not null AUTO_INCREMENT,
	ExteriorColorName varchar(25) not null,
    primary key (ExteriorColorId)
);

CREATE TABLE VehicleTypes (
	VehicleTypeId char(1) not null,
	VehicleTypeName varchar(25) not null,
    primary key (VehicleTypeId)
);

CREATE TABLE Interiors (
	InteriorId int not null AUTO_INCREMENT,
	InteriorName varchar(25) not null,
    primary key (InteriorId)
);

CREATE TABLE PurchaseTypes (
	PurchaseTypeId char(1) not null,
	PurchaseTypeName varchar(50) not null,
    primary key (PurchaseTypeId)
);

CREATE TABLE States (
	StateId varchar(2) not null,
	StateName varchar(15) not null,
    primary key (StateId)
);

CREATE TABLE Vehicles (
	VehicleId int not null AUTO_INCREMENT,
	MakeId int not null,
	ModelId int not null,
	BodyStyleId int not null,
	TransmissionId char(1) not null,
	ExteriorColorId int not null,
	InteriorId int not null,
	VehicleTypeId char(1) not null,
	VehicleYear decimal(4) not null,
	Milage decimal(9,2) not null,
	VinNumber varchar(17) not null,
	Price decimal(8,2) not null,
	MSRP decimal(8,2) not null,
	CreateDate datetime not null,
	ListingDescription varchar(500) not null,
	FeatureVehicle bool not null,
    primary key (VehicleId),
    foreign key(MakeId) references Makes(MakeId),
    foreign key(ModelId) references Models(ModelId),
    foreign key(BodyStyleId) references BodyStyles(BodyStyleId),
    foreign key(TransmissionId) references Transmissions(TransmissionId),
    foreign key(ExteriorColorId) references ExteriorColors(ExteriorColorId),
    foreign key(InteriorId) references Interiors(InteriorId),
    foreign key(VehicleTypeId) references VehicleTypes(VehicleTypeId)
);

CREATE TABLE VehicleImages (
	ImageId int not null AUTO_INCREMENT,
	VehicleId int not null,
	ImageFileName varchar(50) not null,
    primary key (ImageId),
    foreign key(VehicleId) references Vehicles(VehicleId)
);

CREATE TABLE Purchases (
	PurchaseId int not null AUTO_INCREMENT, 
	VehicleId int not null,
	PurchaseName varchar(50) not null,
	Phone varchar(18) null,
	Email varchar(50) null,
	Street1 varchar(50) not null,
	Street2 varchar(50) null,
	City varchar(50) not null,
	StateId varchar(2) not null,
	Zipcode decimal(5) not null,
	PurchasePrice decimal(8,2) not null,
	PurchaseTypeId char(1) not null,
	PurchaseDate datetime not null,
	UserId nvarchar(128)not null,
    primary key (PurchaseId),
    foreign key(VehicleId) references Vehicles(VehicleId),
    foreign key(StateId) references States(StateId),
    foreign key(PurchaseTypeId) references PurchaseTypes(PurchaseTypeId),
    foreign key(UserId) references AspNetUsers(Id)
);

CREATE TABLE Contacts (
	ContactId int not null AUTO_INCREMENT,
	ContactName varchar(50) not null,
	ContactEmail varchar(50) null,
	ContactPhone varchar(18) null,
	ContactMessage varchar(500) null,
    primary key (ContactId)
);

Create Table Specials(
	SpecialId int not null AUTO_INCREMENT,
	SpecialName varchar(50) not null,
	SpecialDescription varchar(500) not null,
	SepcialImageFileName varchar(50) not null,
    primary key (SpecialId)
);