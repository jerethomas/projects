﻿using GuildCars.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;

namespace GuildCars.Data.ADO
{
    class PurchaseTypesRepositoryADO: IPurchaseTypesRepository
    {
        public IEnumerable<PurchaseType> GetAll()
        {
            List<PurchaseType> purchaseType = new List<PurchaseType>();

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("PurchaseTypesSelectAll", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        PurchaseType currentRow = new PurchaseType();
                        currentRow.PurchaseTypeId = dr["PurchaseTypeId"].ToString();
                        currentRow.PurchaseTypeName = dr["PurchaseTypeName"].ToString();

                        purchaseType.Add(currentRow);
                    }
                }
            }

            return purchaseType;
        }
    }
}
