﻿using GuildCars.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;
using System.Data.SqlClient;
using System.Data;
using GuildCars.Models.Queries;
using MySql.Data.MySqlClient;

namespace GuildCars.Data.ADO
{
    public class MakesRepositoryADO : IMakesRepository
    {
        public void AddMake(Make make)
        {
            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("AddMakes", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                MySqlParameter param = new MySqlParameter("MakeIdOUT", MySqlDbType.Int32);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("MakeNameIN", make.MakeName);
                cmd.Parameters.AddWithValue("CreatedDateIN", make.CreatedDate);
                cmd.Parameters.AddWithValue("UserIdIN", make.UserId);

                cn.Open();

                cmd.ExecuteNonQuery();

                make.MakeId = (int)param.Value;
            }
        }

        public IEnumerable<Make> GetAll()
        {
            List<Make> makes = new List<Make>();

            using(var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("MakesSelectAll", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using(MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Make currentRow = new Make();
                        currentRow.MakeId = (int)dr["MakeId"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.CreatedDate = (DateTime)dr["CreatedDate"];
                        currentRow.UserId = dr["UserId"].ToString();

                        makes.Add(currentRow);
                    }
                }
            }

            return makes;
        }

        public IEnumerable<MakeReportItem> GetReport()
        {
            List<MakeReportItem> makes = new List<MakeReportItem>();

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("MakesReport", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        MakeReportItem currentRow = new MakeReportItem();
                        currentRow.MakeId = (int)dr["MakeId"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.CreatedDate = (DateTime)dr["CreatedDate"];
                        currentRow.UserEmail = dr["Email"].ToString();

                        makes.Add(currentRow);
                    }
                }
            }

            return makes;
        }
    }
}