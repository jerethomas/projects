﻿using GuildCars.Data.Interfaces;
using GuildCars.Models.Queries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;
using MySql.Data.MySqlClient;

namespace GuildCars.Data.ADO
{
    public class VehiclesRepositoryADO : IVehiclesRepository
    {

        public string searchString()
        {
            string searchItem = "SELECT v.VehicleId, MakeName, ModelName, BodyStyleName, TransmissionName, ExteriorColorName, InteriorName, VehicleYear, Milage, VehicleTypeName, VinNumber, Price, MSRP, ImageFileName " +
                                "FROM Vehicles v " +
                                        "LEFT JOIN Makes ma ON v.MakeId = ma.MakeId " +
                                        "LEFT JOIN Models mo ON v.ModelId = mo.ModelId " +
                                        "LEFT JOIN BodyStyles bo ON v.BodyStyleId = bo.BodyStyleId " +
                                        "LEFT JOIN Transmissions tr ON v.TransmissionId = tr.TransmissionId " +
                                        "LEFT JOIN ExteriorColors ec ON v.ExteriorColorId = ec.ExteriorColorId " +
                                        "LEFT JOIN Interiors ic ON v.InteriorId = ic.InteriorId " +
                                        "LEFT JOIN VehicleTypes vt ON v.VehicleTypeId = vt.VehicleTypeId " +
                                        "LEFT JOIN Purchases pu ON v.VehicleId = pu.VehicleId " +
                                        "LEFT JOIN VehicleImages vi ON v.VehicleId = vi.VehicleId " +
                               "Where 1 = 1 AND PurchaseId IS NULL ";

            return searchItem;

        }

        public void AddVehicle(Vehicle vehicle)
        {
            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("AddVehicles", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                MySqlParameter param = new MySqlParameter("VehicleIdOUT", MySqlDbType.Int32);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("MakeIdIN", vehicle.MakeId);
                cmd.Parameters.AddWithValue("ModelIdIN", vehicle.ModelId);
                cmd.Parameters.AddWithValue("BodyStyleIdIN", vehicle.BodyStyleId);
                cmd.Parameters.AddWithValue("TransmissionIdIN", vehicle.TransmissionId);
                cmd.Parameters.AddWithValue("ExteriorColorIdIN", vehicle.ExteriorColorId);
                cmd.Parameters.AddWithValue("InteriorIdIN", vehicle.InteriorId);
                cmd.Parameters.AddWithValue("VehicleTypeIdIN", vehicle.VehicleTypeId);
                cmd.Parameters.AddWithValue("VehicleYearIN", vehicle.VehicleYear);
                cmd.Parameters.AddWithValue("MilageIN", vehicle.Milage);
                cmd.Parameters.AddWithValue("VinNumberIN", vehicle.VinNumber);
                cmd.Parameters.AddWithValue("PriceIN", vehicle.Price);
                cmd.Parameters.AddWithValue("MSRPIN", vehicle.MSRP);
                cmd.Parameters.AddWithValue("CreateDateIN", vehicle.CreateDate);
                cmd.Parameters.AddWithValue("ListingDescriptionIN", vehicle.ListingDescription);
                cmd.Parameters.AddWithValue("FeatureVehicleIN", vehicle.FeatureVehicle);

                cn.Open();

                cmd.ExecuteNonQuery();

                vehicle.VehicleId = (int)param.Value;
            }
        }

        public void AddVehicleImage(Image image)
        {
            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("AddVehicleImages", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                MySqlParameter param = new MySqlParameter("ImageIdOUT", MySqlDbType.Int32);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("VehicleIdIN", image.VehicleId);
                cmd.Parameters.AddWithValue("ImageFileNameIN", image.ImageFileName);

                cn.Open();

                cmd.ExecuteNonQuery();

                image.ImageId = (int)param.Value;
            }
        }

        public void DeleteVehicle(int vehicleId)
        {
            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("DeleteVehicles", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("VehicleIdIN", vehicleId);

                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        public void EditVehicle(EditVehicleItem vehicle)
        {
            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("EditVehicles", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("VehicleIdIN", vehicle.VehicleId);
                cmd.Parameters.AddWithValue("MakeIdIN", vehicle.MakeId);
                cmd.Parameters.AddWithValue("ModelIdIN", vehicle.ModelId);
                cmd.Parameters.AddWithValue("BodyStyleIdIN", vehicle.BodyStyleId);
                cmd.Parameters.AddWithValue("TransmissionIdIN", vehicle.TransmissionId);
                cmd.Parameters.AddWithValue("ExteriorColorIdIN", vehicle.ExteriorColorId);
                cmd.Parameters.AddWithValue("InteriorIdIN", vehicle.InteriorId);
                cmd.Parameters.AddWithValue("VehicleTypeIdIN", vehicle.VehicleTypeId);
                cmd.Parameters.AddWithValue("VehicleYearIN", vehicle.VehicleYear);
                cmd.Parameters.AddWithValue("MilageIN", vehicle.Milage);
                cmd.Parameters.AddWithValue("VinNumberIN", vehicle.VinNumber);
                cmd.Parameters.AddWithValue("PriceIN", vehicle.Price);
                cmd.Parameters.AddWithValue("MSRPIN", vehicle.MSRP);
                cmd.Parameters.AddWithValue("ListingDescriptionIN", vehicle.ListingDescription);
                cmd.Parameters.AddWithValue("FeatureVehicleIN", vehicle.FeatureVehicle);
                cmd.Parameters.AddWithValue("ImageFileNameIN", vehicle.ImageFileName);

                cn.Open();

                cmd.ExecuteNonQuery();
            }            
        }

        public VehicleDetailItem GetDetails(string vinNumber)
        {
            VehicleDetailItem vehicle = null;

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("VehiclesSelectDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("VinNumberIN", vinNumber);

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {      
                                 
                    if (dr.Read())
                    {
                        vehicle = new VehicleDetailItem();
                        vehicle.VehicleId = (int)dr["VehicleId"];
                        vehicle.MakeId = (int)dr["MakeId"];
                        vehicle.MakeName = dr["MakeName"].ToString();
                        vehicle.ModelId = (int)dr["ModelId"];
                        vehicle.ModelName = dr["ModelName"].ToString();
                        vehicle.BodyStyleId = (int)dr["BodyStyleId"];
                        vehicle.BodyStyleName = dr["BodyStyleName"].ToString();
                        vehicle.TransmissionId = dr["TransmissionId"].ToString();
                        vehicle.TransmissionName = dr["TransmissionName"].ToString();
                        vehicle.ExteriorColorId = (int)dr["ExteriorColorId"];
                        vehicle.ExteriorColorName = dr["ExteriorColorName"].ToString();
                        vehicle.InteriorId = (int)dr["InteriorId"];                        
                        vehicle.InteriorName = dr["InteriorName"].ToString();
                        vehicle.VehicleYear = (decimal)dr["VehicleYear"];
                        vehicle.Milage = (decimal)dr["Milage"];
                        vehicle.VinNumber = dr["VinNumber"].ToString();
                        vehicle.Price = (decimal)dr["Price"];
                        vehicle.MSRP = (decimal)dr["MSRP"];
                        vehicle.ListingDescription = dr["ListingDescription"].ToString();
                        vehicle.ImageFileName = dr["ImageFileName"].ToString();

                        if (dr["PurchaseId"] != DBNull.Value)
                        {
                            vehicle.PurchaseId = (int)dr["PurchaseId"];
                        }
                    }
                }
            }

            return vehicle;
        }

        public IEnumerable<FeaturedVehicleItem> GetFeatured()
        {
            List<FeaturedVehicleItem> featuredItem = new List<FeaturedVehicleItem>();

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("FeaturedVehiclesSelect", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        FeaturedVehicleItem currentRow = new FeaturedVehicleItem();
                        currentRow.VehicleId = (int)dr["VehicleId"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.VehicleYear = (decimal)dr["VehicleYear"];
                        currentRow.Price = (decimal)dr["Price"];
                        currentRow.VinNumber = dr["VinNumber"].ToString();
                        currentRow.ImageFileName = dr["ImageFileName"].ToString();

                        featuredItem.Add(currentRow);
                    }
                }
            }

            return featuredItem;
        }

        public IEnumerable<VehicleSearchResultItem> VehicleSearchAll(VehicleSearchParameters parameters)
        {
            List<VehicleSearchResultItem> SearchItem = new List<VehicleSearchResultItem>();

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                string query = searchString();

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = cn;

                if (!string.IsNullOrEmpty(parameters.QuickSearch))
                {
                    query += "AND (MakeName LIKE @QuickSearch OR ModelName LIKE @QuickSearch OR VehicleYear LIKE @QuickSearch) ";
                    cmd.Parameters.AddWithValue("@QuickSearch", parameters.QuickSearch + "%");
                }

                if (parameters.MinYear.HasValue)
                {
                    query += "AND VehicleYear >= @MinYear ";
                    cmd.Parameters.AddWithValue("@MinYear", parameters.MinYear);
                }

                if (parameters.MaxYear.HasValue)
                {
                    query += "AND VehicleYear <= @MaxYear ";
                    cmd.Parameters.AddWithValue("@MaxYear", parameters.MaxYear);
                }

                if (parameters.MinPrice.HasValue)
                {
                    query += "AND Price >= @MinPrice ";
                    cmd.Parameters.AddWithValue("@MinPrice", parameters.MinPrice);
                }

                if (parameters.MaxPrice.HasValue)
                {
                    query += "AND Price <= @MaxPrice ";
                    cmd.Parameters.AddWithValue("@MaxPrice", parameters.MaxPrice);
                }

                query += "ORDER BY MSRP DESC LIMIT 20;";

                cmd.CommandText = query;

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        VehicleSearchResultItem currentRow = new VehicleSearchResultItem();
                        currentRow.VehicleId = (int)dr["VehicleId"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.BodyStyleName = dr["BodyStyleName"].ToString();
                        currentRow.TransmissionName = dr["TransmissionName"].ToString();
                        currentRow.ExteriorColorName = dr["ExteriorColorName"].ToString();
                        currentRow.InteriorName = dr["InteriorName"].ToString();
                        currentRow.VehicleYear = (decimal)dr["VehicleYear"];
                        currentRow.Milage = (decimal)dr["Milage"];
                        currentRow.VehicleTypeName = dr["VehicleTypeName"].ToString();
                        currentRow.VinNumber = dr["VinNumber"].ToString();
                        currentRow.Price = (decimal)dr["Price"];
                        currentRow.MSRP = (decimal)dr["MSRP"];
                        currentRow.ImageFileName = dr["ImageFileName"].ToString();

                        SearchItem.Add(currentRow);
                    }
                }
            }

            return SearchItem;
        }

        public IEnumerable<VehicleSearchResultItem> VehicleSearchNew(VehicleSearchParameters parameters)
        {
            List<VehicleSearchResultItem> SearchItem = new List<VehicleSearchResultItem>();

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                string query = searchString() + "AND VehicleTypeName = 'New' ";

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = cn;

                if (!string.IsNullOrEmpty(parameters.QuickSearch))
                {
                    query += "AND (MakeName LIKE @QuickSearch OR ModelName LIKE @QuickSearch OR VehicleYear LIKE @QuickSearch) ";
                    cmd.Parameters.AddWithValue("@QuickSearch", parameters.QuickSearch + "%");
                }

                if (parameters.MinYear.HasValue)
                {
                    query += "AND VehicleYear >= @MinYear ";
                    cmd.Parameters.AddWithValue("@MinYear", parameters.MinYear);
                }

                if (parameters.MaxYear.HasValue)
                {
                    query += "AND VehicleYear <= @MaxYear ";
                    cmd.Parameters.AddWithValue("@MaxYear", parameters.MaxYear);
                }

                if (parameters.MinPrice.HasValue)
                {
                    query += "AND Price >= @MinPrice ";
                    cmd.Parameters.AddWithValue("@MinPrice", parameters.MinPrice);
                }

                if (parameters.MaxPrice.HasValue)
                {
                    query += "AND Price <= @MaxPrice ";
                    cmd.Parameters.AddWithValue("@MaxPrice", parameters.MaxPrice);
                }

                query += "ORDER BY MSRP DESC ";
                cmd.CommandText = query;

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        VehicleSearchResultItem currentRow = new VehicleSearchResultItem();
                        currentRow.VehicleId = (int)dr["VehicleId"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.BodyStyleName = dr["BodyStyleName"].ToString();
                        currentRow.TransmissionName = dr["TransmissionName"].ToString();
                        currentRow.ExteriorColorName = dr["ExteriorColorName"].ToString();
                        currentRow.InteriorName = dr["InteriorName"].ToString();
                        currentRow.VehicleYear = (decimal)dr["VehicleYear"];
                        currentRow.Milage = (decimal)dr["Milage"];
                        currentRow.VehicleTypeName = dr["VehicleTypeName"].ToString();
                        currentRow.VinNumber = dr["VinNumber"].ToString();
                        currentRow.Price = (decimal)dr["Price"];
                        currentRow.MSRP = (decimal)dr["MSRP"];
                        currentRow.ImageFileName = dr["ImageFileName"].ToString();

                        SearchItem.Add(currentRow);
                    }
                }
            }

            return SearchItem;
        }

        public IEnumerable<VehicleSearchResultItem> VehicleSearchUsed(VehicleSearchParameters parameters)
        {
            List<VehicleSearchResultItem> SearchItem = new List<VehicleSearchResultItem>();

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                string query = searchString() + "AND VehicleTypeName = 'Used' ";

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = cn;

                if (!string.IsNullOrEmpty(parameters.QuickSearch))
                {
                    query += "AND (MakeName LIKE @QuickSearch OR ModelName LIKE @QuickSearch OR VehicleYear LIKE @QuickSearch) ";
                    cmd.Parameters.AddWithValue("@QuickSearch", parameters.QuickSearch + "%" );
                }

                if (parameters.MinYear.HasValue)
                {
                    query += "AND VehicleYear >= @MinYear ";
                    cmd.Parameters.AddWithValue("@MinYear", parameters.MinYear);
                }

                if (parameters.MaxYear.HasValue)
                {
                    query += "AND VehicleYear <= @MaxYear ";
                    cmd.Parameters.AddWithValue("@MaxYear", parameters.MaxYear);
                }

                if (parameters.MinPrice.HasValue)
                {
                    query += "AND Price >= @MinPrice ";
                    cmd.Parameters.AddWithValue("@MinPrice", parameters.MinPrice);
                }

                if (parameters.MaxPrice.HasValue)
                {
                    query += "AND Price <= @MaxPrice ";
                    cmd.Parameters.AddWithValue("@MaxPrice", parameters.MaxPrice);
                }

                query += "ORDER BY MSRP DESC ";
                cmd.CommandText = query;

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        VehicleSearchResultItem currentRow = new VehicleSearchResultItem();
                        currentRow.VehicleId = (int)dr["VehicleId"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.BodyStyleName = dr["BodyStyleName"].ToString();
                        currentRow.TransmissionName = dr["TransmissionName"].ToString();
                        currentRow.ExteriorColorName = dr["ExteriorColorName"].ToString();
                        currentRow.InteriorName = dr["InteriorName"].ToString();
                        currentRow.VehicleYear = (decimal)dr["VehicleYear"];
                        currentRow.VehicleTypeName = dr["VehicleTypeName"].ToString();
                        currentRow.Milage = (decimal)dr["Milage"];
                        currentRow.VinNumber = dr["VinNumber"].ToString();
                        currentRow.Price = (decimal)dr["Price"];
                        currentRow.MSRP = (decimal)dr["MSRP"];
                        currentRow.ImageFileName = dr["ImageFileName"].ToString();

                        SearchItem.Add(currentRow);
                    }
                }
            }

            return SearchItem;
        }

        public EditVehicleItem GetVehicleToEdit(string vinNumber)
        {
            EditVehicleItem vehicle = new EditVehicleItem();

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("GetVehicleToEdit", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("VinNumberIN", vinNumber);

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        vehicle.VehicleId = (int)dr["VehicleId"];
                        vehicle.MakeId = (int)dr["MakeId"];
                        vehicle.ModelId = (int)dr["ModelId"];
                        vehicle.BodyStyleId = (int)dr["BodyStyleId"];
                        vehicle.TransmissionId = dr["TransmissionId"].ToString();
                        vehicle.ExteriorColorId = (int)dr["ExteriorColorId"];
                        vehicle.InteriorId = (int)dr["InteriorId"];
                        vehicle.VehicleYear = (decimal)dr["VehicleYear"];
                        vehicle.VehicleTypeId = dr["VehicleTypeId"].ToString();
                        vehicle.VehicleYear = (decimal)dr["VehicleYear"];
                        vehicle.Milage = (decimal)dr["Milage"];
                        vehicle.VinNumber = dr["VinNumber"].ToString();
                        vehicle.Price = (decimal)dr["Price"];
                        vehicle.MSRP = (decimal)dr["MSRP"];
                        vehicle.FeatureVehicle = (bool)dr["FeatureVehicle"];
                        vehicle.ListingDescription = dr["ListingDescription"].ToString();
                        vehicle.ImageFileName = dr["ImageFileName"].ToString();
                    }
                }
            }

            return vehicle;
        }

    }
}