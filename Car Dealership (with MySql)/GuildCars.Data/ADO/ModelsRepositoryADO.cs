﻿using GuildCars.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;
using System.Data.SqlClient;
using System.Data;
using GuildCars.Models.Queries;
using MySql.Data.MySqlClient;

namespace GuildCars.Data.ADO
{
    public class ModelsRepositoryADO : IModelsRepository
    {
        public void AddModel(Model model)
        {
            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("AddModels", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                MySqlParameter param = new MySqlParameter("ModelIdOUT", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("MakeIdIN", model.MakeId);
                cmd.Parameters.AddWithValue("ModelNameIN", model.ModelName);
                cmd.Parameters.AddWithValue("CreatedDateIN", model.CreatedDate);
                cmd.Parameters.AddWithValue("UserIdIN", model.UserId);

                cn.Open();

                cmd.ExecuteNonQuery();

                model.ModelId = (int)param.Value;
            }
        }

        public IEnumerable<Model> GetAll()
        {
            List<Model> model = new List<Model>();

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("ModelsSelectAll", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Model currentRow = new Model();
                        currentRow.MakeId = (int)dr["MakeId"];
                        currentRow.ModelId = (int)dr["ModelId"];
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.CreatedDate = (DateTime)dr["CreatedDate"];
                        currentRow.UserId = dr["UserId"].ToString();

                        model.Add(currentRow);
                    }
                }
            }

            return model;
        }

        public IEnumerable<Model> GetModelByMake(int makeId)
        {
            List<Model> model = new List<Model>();

            using(var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("GetModelByMake", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("MakeIdIN", makeId);

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Model currentRow = new Model();

                        currentRow.ModelId = (int)dr["ModelId"];
                        currentRow.ModelName = dr["ModelName"].ToString();

                        model.Add(currentRow);
                    }
                }
            }

            return model;
        }

        public IEnumerable<ModelReportItem> GetReport()
        {
            List<ModelReportItem> model = new List<ModelReportItem>();

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("ModelsReport", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ModelReportItem currentRow = new ModelReportItem();
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.ModelId = (int)dr["ModelId"];
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.CreatedDate = (DateTime)dr["CreatedDate"];
                        currentRow.UserEmail = dr["Email"].ToString();

                        model.Add(currentRow);
                    }
                }
            }

            return model;
        }
    }
}
