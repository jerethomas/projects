﻿using GuildCars.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;

namespace GuildCars.Data.ADO
{
    public class ContactsRepositoryADO : IContactsRepository
    {
        public void AddContact(Contact contact)
        {
            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("AddContacts", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                MySqlParameter param = new MySqlParameter("ContactIdOUT", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("ContactNameIN", contact.ContactName);
                cmd.Parameters.AddWithValue("ContactMessageIN", contact.ContactMessage);


                if (!string.IsNullOrEmpty(contact.ContactPhone))
                {
                    cmd.Parameters.AddWithValue("ContactPhoneIN", contact.ContactPhone);
                }
                else
                {
                    cmd.Parameters.AddWithValue("ContactPhoneIN", DBNull.Value);
                }

                if (!string.IsNullOrEmpty(contact.ContactEmail))
                {
                    cmd.Parameters.AddWithValue("ContactEmailIN", contact.ContactEmail);
                }
                else
                {
                    cmd.Parameters.AddWithValue("ContactEmailIN", DBNull.Value);
                }

                cn.Open();

                cmd.ExecuteNonQuery();

                contact.ContactId = (int)param.Value;
            }
        }
    }
}
