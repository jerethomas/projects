﻿using GuildCars.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;

namespace GuildCars.Data.ADO
{
    public class PurchasesRepositoryADO : IPurchasesRepository
    {
        public void AddPurchase(Purchase purchase)
        {
            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("AddPurchase", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                MySqlParameter param = new MySqlParameter("PurchaseIdOUT", MySqlDbType.Int32);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("VehicleIdIN", purchase.VehicleId);
                cmd.Parameters.AddWithValue("PurchaseNameIN", purchase.PurchaseName);
                cmd.Parameters.AddWithValue("Street1IN", purchase.Street1);
                cmd.Parameters.AddWithValue("CityIN", purchase.City);
                cmd.Parameters.AddWithValue("StateIdIN", purchase.StateId);
                cmd.Parameters.AddWithValue("ZipcodeIN", purchase.Zipcode);
                cmd.Parameters.AddWithValue("PurchasePriceIN", purchase.PurchasePrice);
                cmd.Parameters.AddWithValue("PurchaseTypeIdIN", purchase.PurchaseTypeId);
                cmd.Parameters.AddWithValue("PurchaseDateIN", purchase.PurchaseDate);
                cmd.Parameters.AddWithValue("UserIdIN", purchase.UserId);


                if (!string.IsNullOrEmpty(purchase.Phone))
                {
                    cmd.Parameters.AddWithValue("PhoneIN", purchase.Phone);
                }
                else
                {
                    cmd.Parameters.AddWithValue("PhoneIN", DBNull.Value);
                }

                if (!string.IsNullOrEmpty(purchase.Email))
                {
                    cmd.Parameters.AddWithValue("EmailIN", purchase.Email);
                }
                else
                {
                    cmd.Parameters.AddWithValue("EmailIN", DBNull.Value);
                }

                if (!string.IsNullOrEmpty(purchase.Street2))
                {
                    cmd.Parameters.AddWithValue("Street2IN", purchase.Street2);
                }
                else
                {
                    cmd.Parameters.AddWithValue("Street2IN", DBNull.Value);
                }

                cn.Open();

                cmd.ExecuteNonQuery();

                purchase.PurchaseId = (int)param.Value;
            }
        }

        public IEnumerable<Purchase> GetAll()
        {
            List<Purchase> purchase = new List<Purchase>();

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("PurchasesSelectAll", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Purchase currentRow = new Purchase();
                        currentRow.PurchaseId = (int)dr["PurchaseId"];
                        currentRow.VehicleId = (int)dr["VehicleId"];
                        currentRow.PurchaseName = dr["PurchaseName"].ToString();
                        currentRow.Phone = dr["Phone"].ToString();
                        currentRow.Email = dr["Email"].ToString();
                        currentRow.Street1 = dr["Street1"].ToString();
                        currentRow.Street2 = dr["Street2"].ToString();
                        currentRow.City = dr["City"].ToString();
                        currentRow.StateId = dr["StateId"].ToString();
                        currentRow.Zipcode = (decimal)dr["Zipcode"];
                        currentRow.PurchasePrice = (decimal)dr["PurchasePrice"];
                        currentRow.PurchaseTypeId = dr["PurchaseTypeId"].ToString();
                        currentRow.PurchaseDate = (DateTime)dr["PurchaseDate"];

                        purchase.Add(currentRow);
                    }
                }
            }

            return purchase;
        }
    }
}
