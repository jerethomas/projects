﻿using GuildCars.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;

namespace GuildCars.Data.ADO
{
    public class TransmissionRepositoryADO : ITransmissionsRepository
    {
        public IEnumerable<Transmission> GetAll()
        {
            List<Transmission> transmissions = new List<Transmission>();

            using (var cn = new MySqlConnection(Settings.GetConnectionString()))
            {
                MySqlCommand cmd = new MySqlCommand("TransmissionsSelectAll", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Transmission currentRow = new Transmission();
                        currentRow.TransmissionId = dr["TransmissionId"].ToString();
                        currentRow.TransmissionName = dr["TransmissionName"].ToString();

                        transmissions.Add(currentRow);
                    }
                }
            }

            return transmissions;
        }
    }
}
