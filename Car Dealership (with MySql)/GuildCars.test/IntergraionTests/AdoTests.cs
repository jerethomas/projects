﻿using GuildCars.Data.ADO;
using GuildCars.Models.Queries;
using GuildCars.Models.Tables;
using MySql.Data.MySqlClient;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildCars.test.IntergraionTests
{
    [TestFixture]
    public class AdoTests
    {
        [SetUp]
        public void Init()
        {
            using (var cn = new MySqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                var cmd = new MySqlCommand();
                cmd.CommandText = "DbReset";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Connection = cn;
                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        [Test]
        public void CanLoadStates()
        {
            var repo = new StatesRepositoryADO();

            var states = repo.GetAll().ToList();

            Assert.AreEqual(4, states.Count);

            Assert.AreEqual("FL", states[0].StateId);
            Assert.AreEqual("Florida", states[0].StateName);
        }

        [Test]
        public void CanLoadBodyStyles()
        {
            var repo = new BodyStylesRepositoryADO();

            var bodyStyle = repo.GetAll().ToList();

            Assert.AreEqual(4, bodyStyle.Count);

            Assert.AreEqual(1, bodyStyle[0].BodyStyleId);
            Assert.AreEqual("Car", bodyStyle[0].BodyStyleName);
        }

        [Test]
        public void CanLoadExteriorColors()
        {
            var repo = new ExteriorColorsRepositoryADO();

            var exteriorColor = repo.GetAll().ToList();

            Assert.AreEqual(5, exteriorColor.Count);

            Assert.AreEqual(1, exteriorColor[0].ExteriorColorId);
            Assert.AreEqual("Red", exteriorColor[0].ExteriorColorName);
        }

        [Test]
        public void CanLoadInteriors()
        {
            var repo = new InteriorRepositoryADO();

            var interior = repo.GetAll().ToList();

            Assert.AreEqual(5, interior.Count);

            Assert.AreEqual(1, interior[0].InteriorId);
            Assert.AreEqual("Cream", interior[0].InteriorName);
        }

        [Test]
        public void CanLoadVehicleTypes()
        {
            var repo = new VehicleTypesRepositoryADO();

            var vehicleTypes = repo.GetAll().ToList();

            Assert.AreEqual(2, vehicleTypes.Count);

            Assert.AreEqual("N", vehicleTypes[0].VehicleTypeId);
            Assert.AreEqual("New", vehicleTypes[0].VehicleTypeName);
        }

        [Test]
        public void CanLoadTransmissions()
        {
            var repo = new TransmissionRepositoryADO();

            var transmissions = repo.GetAll().ToList();

            Assert.AreEqual(2, transmissions.Count);

            Assert.AreEqual("A", transmissions[0].TransmissionId);
            Assert.AreEqual("Automatic", transmissions[0].TransmissionName);
        }

        [Test]
        public void CanLoadPurchases()
        {
            var repo = new PurchasesRepositoryADO();

            var purchase = repo.GetAll().ToList();

            Assert.AreEqual(6, purchase.Count);

            Assert.AreEqual("Eric W.", purchase[0].PurchaseName);
            Assert.AreEqual("789 Somewhere Ave", purchase[0].Street1);
        }

        [Test]
        public void CanLoadMakes()
        {
            var repo = new MakesRepositoryADO();

            var makes = repo.GetAll().ToList();

            Assert.AreEqual(6, makes.Count);

            Assert.AreEqual(1, makes[0].MakeId);
            Assert.AreEqual("Acura", makes[0].MakeName);
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", makes[0].UserId);
        }

        [Test]
        public void CanLoadModels()
        {
            var repo = new ModelsRepositoryADO();

            var models = repo.GetAll().ToList();

            Assert.AreEqual(9, models.Count);

            Assert.AreEqual(1, models[0].ModelId);
            Assert.AreEqual(2, models[0].MakeId);
            Assert.AreEqual("A4", models[0].ModelName);
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", models[0].UserId);
        }

        [Test]
        public void CanLoadModelsByMake()
        {
            var repo = new ModelsRepositoryADO();

            var models = repo.GetModelByMake(6).ToList();

            Assert.AreEqual(2, models.Count);

            Assert.AreEqual(8, models[0].ModelId);
            Assert.AreEqual("Mustang", models[0].ModelName);
        }

        [Test]
        public void CanLoadFeatured()
        {
            var repo = new VehiclesRepositoryADO();

            var featured = repo.GetFeatured().ToList();

            Assert.AreEqual(8, featured.Count);

            Assert.AreEqual(13, featured[6].VehicleId);
        }

        [Test]
        public void CanLoadVehicleDetail()
        {
            var repo = new VehiclesRepositoryADO();

            var detail = repo.GetDetails("p4xvhjmfd5dzqsf68");

            Assert.IsNotNull(detail);

            Assert.AreEqual(6, detail.MakeId);
            Assert.AreEqual("Ford", detail.MakeName);
            Assert.AreEqual(9, detail.ModelId);
            Assert.AreEqual("Edge", detail.ModelName);
            Assert.AreEqual("p4xvhjmfd5dzqsf68", detail.VinNumber);
            Assert.AreEqual(6, detail.PurchaseId);
        }

        [Test]
        public void CanLoadVehicleToEdit()
        {
            var repo = new VehiclesRepositoryADO();

            var editVehicle = repo.GetVehicleToEdit("253daev87qwe65498");

            Assert.IsNotNull(editVehicle);

            Assert.AreEqual(4, editVehicle.MakeId);
            Assert.AreEqual(1, editVehicle.BodyStyleId);
            Assert.AreEqual(6, editVehicle.ModelId);
            Assert.AreEqual("BuickRegal.png", editVehicle.ImageFileName);
            Assert.AreEqual("253daev87qwe65498", editVehicle.VinNumber);
        }

        [Test]
        public void CanAddVehicle()
        {
            Vehicle vehicleToAdd = new Vehicle();
            var repo = new VehiclesRepositoryADO();

            vehicleToAdd.MakeId = 3;
            vehicleToAdd.ModelId = 3;
            vehicleToAdd.BodyStyleId = 3;
            vehicleToAdd.TransmissionId = "A";
            vehicleToAdd.ExteriorColorId = 4;
            vehicleToAdd.InteriorId = 4;
            vehicleToAdd.VehicleTypeId = "N";
            vehicleToAdd.VehicleYear = 1234M;
            vehicleToAdd.Milage = 656.65M;
            vehicleToAdd.VinNumber = "123654asdqweyuio";
            vehicleToAdd.Price = 15000.98M;
            vehicleToAdd.MSRP = 16000.98M;
            vehicleToAdd.CreateDate = DateTime.UtcNow;
            vehicleToAdd.ListingDescription = "test description";
            vehicleToAdd.FeatureVehicle = false;

            repo.AddVehicle(vehicleToAdd);

            Assert.AreEqual(16, vehicleToAdd.VehicleId);
        }

        [Test]
        public void CanAddVehicleImage()
        {
            Image imageToAdd = new Image();
            var repo = new VehiclesRepositoryADO();

            imageToAdd.VehicleId = 3;
            imageToAdd.ImageFileName = "Image.png";


            repo.AddVehicleImage(imageToAdd);

            Assert.AreEqual(16, imageToAdd.ImageId);
        }

        [Test]
        public void CanEditVehicle()
        {
            Vehicle vehicleToAdd = new Vehicle();
            EditVehicleItem vehicleToEdit = new EditVehicleItem();
            Image imageToEdit = new Image();
            var repo = new VehiclesRepositoryADO();

            vehicleToAdd.MakeId = 3;
            vehicleToAdd.ModelId = 3;
            vehicleToAdd.BodyStyleId = 3;
            vehicleToAdd.TransmissionId = "A";
            vehicleToAdd.ExteriorColorId = 4;
            vehicleToAdd.InteriorId = 4;
            vehicleToAdd.VehicleTypeId = "N";
            vehicleToAdd.VehicleYear = 1234M;
            vehicleToAdd.Milage = 656.65M;
            vehicleToAdd.VinNumber = "123654asdqweyuio";
            vehicleToAdd.Price = 15000.98M;
            vehicleToAdd.MSRP = 16000.98M;
            vehicleToAdd.CreateDate = DateTime.Now;
            vehicleToAdd.ListingDescription = "test description";
            vehicleToAdd.FeatureVehicle = false;

            repo.AddVehicle(vehicleToAdd);

            Assert.AreEqual(16, vehicleToAdd.VehicleId);

            imageToEdit.ImageFileName = "image.png";
            imageToEdit.VehicleId = vehicleToAdd.VehicleId;

            repo.AddVehicleImage(imageToEdit);

            Assert.AreEqual(16, imageToEdit.ImageId);

            vehicleToEdit.VehicleId = 13;
            vehicleToEdit.MakeId = 2;
            vehicleToEdit.ModelId = 2;
            vehicleToEdit.BodyStyleId = 2;
            vehicleToEdit.TransmissionId = "A";
            vehicleToEdit.ExteriorColorId = 4;
            vehicleToEdit.InteriorId = 4;
            vehicleToEdit.VehicleTypeId = "U";
            vehicleToEdit.VehicleYear = 1234M;
            vehicleToEdit.Milage = 1656.65M;
            vehicleToEdit.VinNumber = "123654asdqweyuio";
            vehicleToEdit.Price = 14000.98M;
            vehicleToEdit.MSRP = 15000.98M;
            vehicleToEdit.ListingDescription = "test description update";
            vehicleToEdit.FeatureVehicle = true;
            vehicleToEdit.ImageFileName = "updated.png";

            repo.EditVehicle(vehicleToEdit);

            var updatedVehichle = repo.GetVehicleToEdit("123654asdqweyuio");

            Assert.AreEqual(1656.65M, updatedVehichle.Milage);
            Assert.AreEqual(14000.98M, updatedVehichle.Price);
            Assert.AreEqual(15000.98M, updatedVehichle.MSRP);
            Assert.AreEqual("test description update", updatedVehichle.ListingDescription);
            Assert.AreEqual("updated.png", updatedVehichle.ImageFileName);
        }

        [Test]
        public void CanDeleteVehicle()
        {
            Vehicle vehicleToDelete = new Vehicle();
            Image imageToDelete = new Image();

            var repo = new VehiclesRepositoryADO();

            vehicleToDelete.MakeId = 3;
            vehicleToDelete.ModelId = 3;
            vehicleToDelete.BodyStyleId = 3;
            vehicleToDelete.TransmissionId = "A";
            vehicleToDelete.ExteriorColorId = 4;
            vehicleToDelete.InteriorId = 4;
            vehicleToDelete.VehicleTypeId = "N";
            vehicleToDelete.VehicleYear = 1234M;
            vehicleToDelete.Milage = 656.65M;
            vehicleToDelete.VinNumber = "12345698765432145";
            vehicleToDelete.Price = 15000.98M;
            vehicleToDelete.MSRP = 16000.98M;
            vehicleToDelete.CreateDate = DateTime.Now;
            vehicleToDelete.ListingDescription = "test description";
            vehicleToDelete.FeatureVehicle = false;

            repo.AddVehicle(vehicleToDelete);

            imageToDelete.ImageFileName = "image.png";
            imageToDelete.VehicleId = vehicleToDelete.VehicleId;

            repo.AddVehicleImage(imageToDelete);

            var checkVehicle = repo.GetDetails("12345698765432145");

            Assert.IsNotNull(checkVehicle);
            Assert.AreEqual(16, checkVehicle.VehicleId);
            Assert.AreEqual("12345698765432145", checkVehicle.VinNumber);

            repo.DeleteVehicle(16);

            var newcheckVehicle = repo.GetDetails("12345698765432145");

            Assert.IsNull(newcheckVehicle);
        }

        [Test]
        public void CanAddMake()
        {
            Make makeToAdd = new Make();
            var repo = new MakesRepositoryADO();

            makeToAdd.MakeName = "Test";
            makeToAdd.CreatedDate = DateTime.Now;
            makeToAdd.UserId = "22222222-1111-1111-1111-111111111111";

            repo.AddMake(makeToAdd);

            Assert.AreEqual(7, makeToAdd.MakeId);
        }

        [Test]
        public void CanAddModel()
        {
            Model modelToAdd = new Model();
            var repo = new ModelsRepositoryADO();

            modelToAdd.ModelName = "Test Model";
            modelToAdd.MakeId = 2;
            modelToAdd.CreatedDate = DateTime.Now;
            modelToAdd.UserId = "22222222-1111-1111-1111-111111111111";

            repo.AddModel(modelToAdd);

            Assert.AreEqual(10, modelToAdd.ModelId);
        }

        [Test]
        public void CanLoadSpecials()
        {
            var repo = new SpecialsRepositoryADO();

            var special = repo.GetAll().ToList();

            Assert.AreEqual(3, special.Count);

            Assert.AreEqual(1, special[0].SpecialId);
            Assert.AreEqual("$1 Down!", special[0].SpecialName);
            Assert.AreEqual("$1 Gets you a slightly used car!", special[0].SpecialDescription);
            Assert.AreEqual("1dollar.png", special[0].SpecialImageFileName);
        }

        [Test]
        public void CanAddSpecial()
        {
            Special specialToAdd = new Special();
            var repo = new SpecialsRepositoryADO();

            specialToAdd.SpecialName = "Test special";
            specialToAdd.SpecialDescription = "This is a test special";
            specialToAdd.SpecialImageFileName = "testspecial.png";

            repo.AddSpecial(specialToAdd);

            Assert.AreEqual(4, specialToAdd.SpecialId);
        }

        [Test]
        public void CanDeleteSpecial()
        {
            Special specialToDelete = new Special();
            var repo = new SpecialsRepositoryADO();

            specialToDelete.SpecialName = "Test special";
            specialToDelete.SpecialDescription = "This is a test special";
            specialToDelete.SpecialImageFileName = "testspecial.png";

            repo.AddSpecial(specialToDelete);

            var checkSpecial = repo.GetAll();
            Assert.AreEqual(4, checkSpecial.Count());

            repo.DeleteSpecial(4);

            checkSpecial = repo.GetAll();

            Assert.AreEqual(3, checkSpecial.Count());
        }

        [Test]
        public void CanAddContact()
        {
            Contact ContactToAdd = new Contact();
            var repo = new ContactsRepositoryADO();

            ContactToAdd.ContactName = "Mr testy";
            ContactToAdd.ContactPhone = "3214659870";
            ContactToAdd.ContactEmail = "testy@test.com";
            ContactToAdd.ContactMessage = "Give me car!";

            repo.AddContact(ContactToAdd);

            Assert.AreEqual(4, ContactToAdd.ContactId);
        }

        [Test]
        public void CanAddPurchase()
        {
            Purchase purchaseToAdd = new Purchase();
            var repo = new PurchasesRepositoryADO();

            purchaseToAdd.VehicleId = 2;
            purchaseToAdd.PurchaseName = "Big Fred";
            purchaseToAdd.Phone = "1234567890";
            purchaseToAdd.Email = "Give@mecar.com";
            purchaseToAdd.Street1 = "i live here";
            purchaseToAdd.City = "big town";
            purchaseToAdd.StateId = "FL";
            purchaseToAdd.Zipcode = 12341m;
            purchaseToAdd.PurchasePrice = 131231m;
            purchaseToAdd.PurchaseTypeId = "B";
            purchaseToAdd.PurchaseDate = DateTime.Now;
            purchaseToAdd.UserId = "00000000-1111-1111-1111-111111111111";

            repo.AddPurchase(purchaseToAdd);

            Assert.AreEqual(7, purchaseToAdd.PurchaseId);
        }

        [Test]
        public void CanSearchAllVehiclesMakes()
        {
            var repo = new VehiclesRepositoryADO();

            var found = repo.VehicleSearchAll(new VehicleSearchParameters { QuickSearch = "Ford"});

            Assert.AreEqual(2, found.Count());

            found = repo.VehicleSearchAll(new VehicleSearchParameters { QuickSearch = "2017" });

            Assert.AreEqual(2, found.Count());

            found = repo.VehicleSearchAll(new VehicleSearchParameters { QuickSearch = "2017" });

            Assert.AreEqual(2, found.Count());
        }

        [Test]
        public void CanSearchNewVehiclesMakes()
        {
            var repo = new VehiclesRepositoryADO();

            var found = repo.VehicleSearchNew(new VehicleSearchParameters { QuickSearch = "2017" });

            Assert.AreEqual(2, found.Count());
        }

        [Test]
        public void CanSearchAllVehiclesMinYear()
        {
            var repo = new VehiclesRepositoryADO();

            var found = repo.VehicleSearchAll(new VehicleSearchParameters { MinYear = 2008m });

            Assert.AreEqual(8, found.Count());
        }

        [Test]
        public void CanSearchNewVehiclesMinYear()
        {
            var repo = new VehiclesRepositoryADO();

            var found = repo.VehicleSearchNew(new VehicleSearchParameters { MinYear = 2017m });

            Assert.AreEqual(5, found.Count());
        }

        [Test]
        public void CanSearchAllVehiclesMaxPrice()
        {
            var repo = new VehiclesRepositoryADO();

            var found = repo.VehicleSearchAll(new VehicleSearchParameters { MaxPrice = 50000m });

            Assert.AreEqual(6, found.Count());
        }

        [Test]
        public void CanSearchNewVehiclesMaxPrice()
        {
            var repo = new VehiclesRepositoryADO();

            var found = repo.VehicleSearchNew(new VehicleSearchParameters { MaxPrice = 50000m });

            Assert.AreEqual(4, found.Count());
        }

        [Test]
        public void CanSearchAllSalesReport()
        {
            var repo = new ReportsRepositoryADO();

            var found = repo.SalesReport(new SalesSearchParameters { UserId = "00000000-1111-1111-1111-111111111111" }).ToList();

            Assert.AreEqual(1, found.Count());
            Assert.AreEqual("User3", found[0].FirstName);
            Assert.AreEqual(150400.25m, found[0].TotalSales);

            found = repo.SalesReport(new SalesSearchParameters { UserId = "11111111-1111-1111-1111-111111111111" }).ToList();

            Assert.AreEqual(1, found.Count());
            Assert.AreEqual("User1", found[0].FirstName);
            Assert.AreEqual(987654.12m, found[0].TotalSales);

            found = repo.SalesReport(new SalesSearchParameters { UserId = "35591c59-5324-4463-8eed-b4d594b4bc6b" }).ToList();

            Assert.AreEqual(1, found.Count());
            Assert.AreEqual("test", found[0].FirstName);
            Assert.AreEqual(400.24m, found[0].TotalSales);
        }

        [Test]
        public void CanGetNewVehicleReport()
        {
            var repo = new ReportsRepositoryADO();

            var found = repo.NewVehicleReport();

            Assert.AreEqual(6, found.Count());
        }

        [Test]
        public void CanSearchSalesReportByDate()
        {
            var repo = new ReportsRepositoryADO();

            var found = repo.SalesReport(new SalesSearchParameters { UserId = "00000000-1111-1111-1111-111111111111" }).ToList();

            Assert.AreEqual(1, found.Count());
            Assert.AreEqual("User3", found[0].FirstName);
            Assert.AreEqual(150400.25m, found[0].TotalSales);

            found = repo.SalesReport(new SalesSearchParameters { UserId = "11111111-1111-1111-1111-111111111111" }).ToList();

            Assert.AreEqual(1, found.Count());
            Assert.AreEqual("User1", found[0].FirstName);
            Assert.AreEqual(987654.12m, found[0].TotalSales);

            found = repo.SalesReport(new SalesSearchParameters { UserId = "35591c59-5324-4463-8eed-b4d594b4bc6b" }).ToList();

            Assert.AreEqual(1, found.Count());
            Assert.AreEqual("test", found[0].FirstName);
            Assert.AreEqual(400.24m, found[0].TotalSales);
        }
    }
}
