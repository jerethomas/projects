﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuildCars.UI.Models
{
    public class SearchFieldsModel
    {        
        public List<decimal> MinPriceDropdownValue { get; set; }
        public List<decimal> MaxPriceDropdownValue { get; set; }
        public List<decimal> YearDropDownValue { get; set; }
    }
}