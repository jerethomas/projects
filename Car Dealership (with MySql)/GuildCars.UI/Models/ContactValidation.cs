﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace GuildCars.UI.Models
{
    public class ContactValidation: IValidatableObject
    {
        [Required]
        public int ContactId { get; set; }

        public string ContactName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string ContactEmail { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string ContactPhone { get; set; }

        [Required]
        public string ContactMessage { get; set; }

        public static bool IsPhoneNumber(string ContactPhone)
        {
            if (Regex.Match(ContactPhone, @"1?[\s.-]?\(?(\d{3})\)?[\s.-]?\d{3}[\s.-]?\d{4}").Success)
            {
                return true;
            }

            return false;
        }

        public static bool IsEmail(string ContactEmail)
        {
            if (Regex.Match(ContactEmail, @"(.*)@(.*)\.(.*)").Success)
            {
                return true;
            }

            return false;
        }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(ContactEmail) && (string.IsNullOrEmpty(ContactPhone)))
            {
                errors.Add(new ValidationResult("A contact Phone number or Email address must be provided."));
            }

            bool validPhoneNumber = false;
            bool validEmail = false;

            if (!string.IsNullOrEmpty(ContactPhone))
            {
                validPhoneNumber = IsPhoneNumber(ContactPhone);
            }

            if (!string.IsNullOrEmpty(ContactEmail))
            {
                validEmail = IsEmail(ContactEmail);
            }

            if (!validPhoneNumber)
            {
                errors.Add(new ValidationResult("Please enter a valid phone number."));
            }

            if (!validEmail)
            {
                errors.Add(new ValidationResult("Please enter a valid Email."));
            }

            if (!validPhoneNumber && string.IsNullOrEmpty(ContactEmail))
            {
                errors.Add(new ValidationResult("Please enter a valid phone number or put in an email address."));
            }

            if (!validEmail && string.IsNullOrEmpty(ContactPhone))
            {
                errors.Add(new ValidationResult("Please enter a valid Email or put in a phone number."));
            }

            if (string.IsNullOrEmpty(ContactName))
            {
                errors.Add(new ValidationResult("Please enter a valid Name for contact."));
            }

            if (string.IsNullOrEmpty(ContactMessage))
            {
                errors.Add(new ValidationResult("Please enter a message."));
            }

            return errors;
        }
    }
}