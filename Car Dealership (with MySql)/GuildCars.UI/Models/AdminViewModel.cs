﻿using GuildCars.Data.Factories;
using GuildCars.Models.Queries;
using GuildCars.Models.Tables;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.WebPages.Html;

namespace GuildCars.UI.Models
{

    public class AddVehicleViewModel: IValidatableObject
    {
        public Vehicle Vehicle { get; set; }
        public IEnumerable<Make> Makes { get; set; }
        public IEnumerable<Model> Models { get; set; }
        public IEnumerable<VehicleType> VehicleTypes { get; set; }
        public IEnumerable<BodyStyle> BodyStyles { get; set; }
        public IEnumerable<Transmission> Transmissionns { get; set; }
        public IEnumerable<ExteriorColor> ExteriorColors { get; set; }
        public IEnumerable<Interior> Interiors { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }

        public static bool IsVinNumber(string VinNumber)
        {
            if (Regex.Match(VinNumber, @"([A-HJ-NPR-Za-hj-npr-z0-9]{12})([0-9]{5})").Success)
            {
                return true;
            }

            return false;
        }

        public static bool VinExists(string VinNumber)
        {
            var repo = VehiclesRepositoryFactory.GetRepository();

            if (VinNumber != null)
            {

                var getVehicleToCheck = repo.GetDetails(VinNumber);

                if (getVehicleToCheck == null)
                {
                    return true;
                }
            }
           
            return false;
        }                    

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            bool validVinNumber = false;
            bool vinExists = false;

            if (!string.IsNullOrEmpty(Vehicle.VinNumber))
            {
                validVinNumber = IsVinNumber(Vehicle.VinNumber);
            }

            if (!validVinNumber)
            {
                errors.Add(new ValidationResult("Please enter a valid VIN number."));
            }

            if (!string.IsNullOrEmpty(Vehicle.VinNumber))
            {
                vinExists = VinExists(Vehicle.VinNumber);
            }

            if (!vinExists)
            {
                errors.Add(new ValidationResult("That VIN number is already in the system."));
            }

            if (string.IsNullOrEmpty(Vehicle.VinNumber))
            {
                errors.Add(new ValidationResult("Please enter a VIN number."));
            }

            if (string.IsNullOrEmpty(Vehicle.VehicleYear.ToString()))
            {
                errors.Add(new ValidationResult("Please enter a valid year."));
            }

            if (Vehicle.VehicleYear < 2000)
            {
                errors.Add(new ValidationResult("The Year cannot be lower than 2000."));
            }

            if (Vehicle.VehicleYear > (DateTime.Now.Year + 1))
            {
                errors.Add(new ValidationResult($"The Year cannot be greater than {DateTime.Now.Year + 1}."));
            }

            if (string.IsNullOrEmpty(Vehicle.Milage.ToString()))
            {
                errors.Add(new ValidationResult("Please enter a the vehicle Mileage."));
            }

            if (Vehicle.Milage < 1)
            {
                errors.Add(new ValidationResult("Milage must be a positive number"));
            }

            if (Vehicle.Milage >= 1000 && Vehicle.VehicleTypeId == "N")
            {
                errors.Add(new ValidationResult("New Vehichles can not have 1000 or more Miles."));
            }

            if (Vehicle.Milage <= 1000 && Vehicle.VehicleTypeId == "U")
            {
                errors.Add(new ValidationResult("Used Vehichles can not have less than 1000 Miles."));
            }

            if (string.IsNullOrEmpty(Vehicle.ListingDescription))
            {
                errors.Add(new ValidationResult("Description is required."));
            }

            if (Vehicle.MSRP < 1 )
            {
                errors.Add(new ValidationResult("Please enter a valid vehicle MSRP."));
            }

            if (Vehicle.Price < 1)
            {
                errors.Add(new ValidationResult("Please enter a valid vehicle Price."));
            }

            if (Vehicle.Price > Vehicle.MSRP)
            {
                errors.Add(new ValidationResult("The Vehicle Price cannot be greater than its MSRP."));
            }

            if (ImageUpload == null || ImageUpload.ContentLength < 0)
            {
                errors.Add(new ValidationResult("An Image of the vehicle is required."));
            }

            if (ImageUpload != null || ImageUpload.ContentLength > 0)
            {
                var acceptedExtentions = new string[] { ".png", ".jpg", ".jpeg" };

                var imageExtention = Path.GetExtension(ImageUpload.FileName);

                if (!acceptedExtentions.Contains(imageExtention))
                {
                    errors.Add(new ValidationResult("The uploaded image must be a jpg, png or jpeg."));
                }

            }

            return errors;
        }
    }

    public class EditVehicleViewModel: IValidatableObject
    {
        public EditVehicleItem Vehicle { get; set; }
        public IEnumerable<Make> Makes { get; set; }
        public IEnumerable<Model> Models { get; set; }
        public IEnumerable<VehicleType> VehicleTypes { get; set; }
        public IEnumerable<BodyStyle> BodyStyles { get; set; }
        public IEnumerable<Transmission> Transmissionns { get; set; }
        public IEnumerable<ExteriorColor> ExteriorColors { get; set; }
        public IEnumerable<Interior> Interiors { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }
        public string CurrentVinNumber { get; set; }

        public static bool IsVinNumber(string VinNumber)
        {
            if (Regex.Match(VinNumber, @"([A-HJ-NPR-Za-hj-npr-z0-9]{12})([0-9]{5})").Success)
            {
                return true;
            }

            return false;
        }

        public static bool VinExists(string VinNumber)
        {
            var repo = VehiclesRepositoryFactory.GetRepository().GetDetails(VinNumber);


            if (repo == null)
            {
                return true;
            }

            return false;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            bool validVinNumber = false;
            bool vinExists = true;

            if (!string.IsNullOrEmpty(Vehicle.VinNumber))
            {
                validVinNumber = IsVinNumber(Vehicle.VinNumber);
            }

            if (!validVinNumber)
            {
                errors.Add(new ValidationResult("Please enter a valid VIN number."));
            }

            if (string.IsNullOrEmpty(Vehicle.VinNumber))
            {
                errors.Add(new ValidationResult("Please enter a VIN number."));
            }

            if (!string.IsNullOrEmpty(Vehicle.VinNumber) && Vehicle.VinNumber != CurrentVinNumber)
            {
                vinExists = VinExists(Vehicle.VinNumber);
            }

            if (!vinExists)
            {
                errors.Add(new ValidationResult("That VIN number is already in the system."));
            }

            if (string.IsNullOrEmpty(Vehicle.VehicleYear.ToString()))
            {
                errors.Add(new ValidationResult("Please enter a valid year."));
            }

            if (Vehicle.VehicleYear < 2000)
            {
                errors.Add(new ValidationResult("The Year cannot be lower than 2000."));
            }

            if (Vehicle.VehicleYear > (DateTime.Now.Year + 1))
            {
                errors.Add(new ValidationResult($"The Year cannot be greater than {DateTime.Now.Year + 1}."));
            }

            if (string.IsNullOrEmpty(Vehicle.Milage.ToString()))
            {
                errors.Add(new ValidationResult("Please enter a the vehicle Mileage."));
            }

            if (Vehicle.Milage < 1)
            {
                errors.Add(new ValidationResult("Milage must be a positive number"));
            }

            if (Vehicle.Milage >= 1000 && Vehicle.VehicleTypeId == "N")
            {
                errors.Add(new ValidationResult("New Vehichles can not have 1000 or more Miles."));
            }

            if (Vehicle.Milage <= 1000 && Vehicle.VehicleTypeId == "U")
            {
                errors.Add(new ValidationResult("Used Vehichles can must have more than 1000 Miles."));
            }

            if (string.IsNullOrEmpty(Vehicle.ListingDescription))
            {
                errors.Add(new ValidationResult("Description is required."));
            }

            if (Vehicle.MSRP < 1)
            {
                errors.Add(new ValidationResult("Please enter a valid vehicle MSRP."));
            }

            if (Vehicle.Price < 1)
            {
                errors.Add(new ValidationResult("Please enter a valid vehicle Price."));
            }

            if (Vehicle.Price > Vehicle.MSRP)
            {
                errors.Add(new ValidationResult("The Vehicle Price cannot be greater than its MSRP."));
            }

            if (ImageUpload != null && ImageUpload.ContentLength > 0)
            {
                var acceptedExtentions = new string[] { ".png", ".jpg", ".jpeg" };

                var imageExtention = Path.GetExtension(ImageUpload.FileName);

                if (!acceptedExtentions.Contains(imageExtention))
                {
                    errors.Add(new ValidationResult("The uploaded image must be a jpg, png or jpeg."));
                }

            }

            return errors;
        }
    } 

    public class RoleItem
    {
        public string RoleName { get; set; }
        public string RoleId { get; set; }
    }

    public class UserItem
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
        public IEnumerable<string> RoleList { get; set; }
    }

    public class SalesReportUserItem
    {
        public string Name { get; set; }
        public string UserId { get; set; }
    }

    public class AddUserViewModel
    {
        [Required(AllowEmptyStrings = false)]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public List<RoleItem> Roles { get; set; }

        [Required]
        public string Role { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
    }

    public class EditUserViewModel
    {
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        public string CurrentEmail { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string LastName { get; set; }

        public List<RoleItem> Roles { get; set; }

        [Required]
        public string Role { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "NewPassword")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }    

    public class ChangePassword
    {        
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class SelectUserRolesViewModel
    {
        public SelectUserRolesViewModel()
        {
            this.Roles = new List<SelectRoleEditorViewModel>();
        }


        // Enable initialization with an instance of ApplicationUser:
        public SelectUserRolesViewModel(ApplicationUser user) : this()
        {
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;

            var Db = new ApplicationDbContext();

            // Add all available roles to the list of EditorViewModels:
            var allRoles = Db.Roles;
            foreach (var role in allRoles)
            {
                // An EditorViewModel will be used by Editor Template:
                var rvm = new SelectRoleEditorViewModel(role);
                this.Roles.Add(rvm);
            }

            // Set the Selected property to true for those roles for 
            // which the current user is a member:
            foreach (var userRole in user.Roles)
            {
                var checkUserRole =
                    //this.Roles.Find(r => r.RoleName == userRole.Role.Name);
                    this.Roles.Find(r => r.RoleId == userRole.RoleId);
                checkUserRole.Selected = true;
            }
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<SelectRoleEditorViewModel> Roles { get; set; }
    }

    public class SelectRoleEditorViewModel
    {
        //added from article

        public SelectRoleEditorViewModel(IdentityRole role)
        {
            this.RoleId = role.Id;
        }

        public bool Selected { get; set; }

        [Required]
        public string RoleId { get; set; }
    }

    public class SalesReportUserItemList
    {
        public List<SalesReportUserItem> UserItemForSalesReport { get; set; }
    }
}