﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuildCars.UI.Models
{
    public class LoanCalculatorViewModel
    {
        public double LoanPrincipal { get; set; }
        public double InterestRate { get; set; }
        public double LoanLengthInMonths { get; set; }
        public double LoanLengthInYears { get; set; }
        public double MonthlyPayments { get; set; }
        public double TotalLoanAmount { get; set; }
        public double TotalInterestPaid { get; set; }

        public double CompareLoanPrincipal { get; set; }
        public double CompareInterestRate { get; set; }
        public double CompareLoanLengthInMonths { get; set; }
        public double CompareLoanLengthInYears { get; set; }

        public double CompareMonthlyPayments { get; set; }
        public double CompareTotalLoanAmount { get; set; }
        public double CompareTotalInterestPaid { get; set; }

        public double DiffMonthlyPayments { get; set; }
        public double DiffTotalLoanAmount { get; set; }
        public double DiffTotalInterestPaid { get; set; }
    }
}