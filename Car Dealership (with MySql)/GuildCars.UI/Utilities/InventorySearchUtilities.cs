﻿using GuildCars.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuildCars.UI.Utilities
{
    public class InventorySearchUtilities
    {
        public static SearchFieldsModel searchModelValues()
        {
            SearchFieldsModel model = new SearchFieldsModel();

            List<decimal> minPriceValue = new List<decimal>();
            List<decimal> maxPriceValue = new List<decimal>();
            List<decimal> yearValue = new List<decimal>();

            for (decimal i = 10000m; i <= 50000m; i += 5000m)
            {
                minPriceValue.Add(i);
            }

            for (decimal i = 50000m; i <= 100000m; i += 5000m)
            {
                maxPriceValue.Add(i);
            }

            for (decimal i = 2000m; i <= DateTime.Now.Year + 1m; i++)
            {
                yearValue.Add(i);
            }

            model.MinPriceDropdownValue = minPriceValue;
            model.MaxPriceDropdownValue = maxPriceValue;
            model.YearDropDownValue = yearValue;

            return model;
        }
    }
}