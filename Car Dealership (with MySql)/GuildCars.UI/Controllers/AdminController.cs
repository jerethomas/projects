﻿using GuildCars.UI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using System.Net;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;
using static GuildCars.UI.Controllers.ManageController;
using GuildCars.Data.Factories;
using GuildCars.Models.Tables;
using System.IO;
using GuildCars.Models.Queries;
using GuildCars.UI.Utilities;

namespace GuildCars.UI.Controllers
{
    public class AdminController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult Admin()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Vehicles()
        {
            var model = Utilities.InventorySearchUtilities.searchModelValues();

            return View(model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult AddVehicle()
        {
            var model = new AddVehicleViewModel();

            var makesRepo = MakesRepositoryFactory.GetRepository();
            var modelsRepo = ModelsRepositoryFactory.GetRepository();
            var vehichleType = VehicleTypesRepositoryFactory.GetRepository();
            var bodyStylesRepo = BodyStylesRepositoryFactory.GetRepository();
            var transmissionReop = TransmissionRepositoryFactory.GetRepository();
            var exteriorColor = ExteriorColorsRepositoryFactory.GetRepository();
            var interior = InteriorRepositoryFactory.GetRepository();

            model.Vehicle = new Vehicle();            
            model.Makes = makesRepo.GetAll();
            model.Models = modelsRepo.GetAll();
            model.VehicleTypes = vehichleType.GetAll();
            model.BodyStyles = bodyStylesRepo.GetAll();
            model.Transmissionns = transmissionReop.GetAll();
            model.ExteriorColors = exteriorColor.GetAll();
            model.Interiors = interior.GetAll();

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult AddVehicle(AddVehicleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var repo = VehiclesRepositoryFactory.GetRepository();

                model.Vehicle.CreateDate = DateTime.UtcNow;

                try
                {
                    repo.AddVehicle(model.Vehicle);

                    if (model.ImageUpload != null && model.ImageUpload.ContentLength > 0 )
                    {
                        Image image = new Image();

                        var savePath = Server.MapPath("~/Images");

                        string extention = Path.GetExtension(model.ImageUpload.FileName);

                        var filePath = Path.Combine(savePath, "inventory-" + model.Vehicle.VehicleId.ToString() + extention);

                        model.ImageUpload.SaveAs(filePath);
                        image.ImageFileName = Path.GetFileName(filePath);
                        image.VehicleId = model.Vehicle.VehicleId;

                        repo.AddVehicleImage(image);
                    }

                    return RedirectToAction("EditVehicle", "Admin", new { vinNumber = model.Vehicle.VinNumber });
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            else
            {
                var makesRepo = MakesRepositoryFactory.GetRepository();
                var modelsRepo = ModelsRepositoryFactory.GetRepository();
                var vehichleType = VehicleTypesRepositoryFactory.GetRepository();
                var bodyStylesRepo = BodyStylesRepositoryFactory.GetRepository();
                var transmissionReop = TransmissionRepositoryFactory.GetRepository();
                var exteriorColor = ExteriorColorsRepositoryFactory.GetRepository();
                var interior = InteriorRepositoryFactory.GetRepository();

                model.Vehicle = new Vehicle();
                model.Makes = makesRepo.GetAll();
                model.Models = modelsRepo.GetAll();
                model.VehicleTypes = vehichleType.GetAll();
                model.BodyStyles = bodyStylesRepo.GetAll();
                model.Transmissionns = transmissionReop.GetAll();
                model.ExteriorColors = exteriorColor.GetAll();
                model.Interiors = interior.GetAll();

                return View(model);
            }
            
        }

        [Authorize(Roles = "admin")]
        public ActionResult EditVehicle(string vinNumber)
        {
            var repo = VehiclesRepositoryFactory.GetRepository();

            var model = new EditVehicleViewModel();

            var makesRepo = MakesRepositoryFactory.GetRepository();
            var modelsRepo = ModelsRepositoryFactory.GetRepository();
            var vehichleType = VehicleTypesRepositoryFactory.GetRepository();
            var bodyStylesRepo = BodyStylesRepositoryFactory.GetRepository();
            var transmissionReop = TransmissionRepositoryFactory.GetRepository();
            var exteriorColor = ExteriorColorsRepositoryFactory.GetRepository();
            var interior = InteriorRepositoryFactory.GetRepository();

            model.Vehicle = repo.GetVehicleToEdit(vinNumber);
            model.Makes = makesRepo.GetAll();
            model.Models = modelsRepo.GetAll();
            model.VehicleTypes = vehichleType.GetAll();
            model.BodyStyles = bodyStylesRepo.GetAll();
            model.Transmissionns = transmissionReop.GetAll();
            model.ExteriorColors = exteriorColor.GetAll();
            model.Interiors = interior.GetAll();
            model.CurrentVinNumber = model.Vehicle.VinNumber;

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult EditVehicle(EditVehicleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var repo = VehiclesRepositoryFactory.GetRepository();

                try
                {                    

                    var oldVehicle = repo.GetVehicleToEdit(model.CurrentVinNumber);

                    if (model.ImageUpload != null && model.ImageUpload.ContentLength > 0)
                    {

                        var savePath = Server.MapPath("~/Images");

                        var oldImage = Path.Combine(savePath, oldVehicle.ImageFileName);

                        if (System.IO.File.Exists(oldImage))
                        {
                            System.IO.File.Delete(oldImage);
                        }

                        string extention = Path.GetExtension(model.ImageUpload.FileName);

                        var filePath = Path.Combine(savePath, "inventory-" + model.Vehicle.VehicleId.ToString() + extention);

                        model.ImageUpload.SaveAs(filePath);
                        model.Vehicle.ImageFileName = Path.GetFileName(filePath);

                    }
                    else
                    {
                        model.Vehicle.ImageFileName = oldVehicle.ImageFileName;
                    }

                    repo.EditVehicle(model.Vehicle);

                    return RedirectToAction("Vehicles", "Admin");
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            else
            {

                var repo = VehiclesRepositoryFactory.GetRepository();

                var makesRepo = MakesRepositoryFactory.GetRepository();
                var modelsRepo = ModelsRepositoryFactory.GetRepository();
                var vehichleType = VehicleTypesRepositoryFactory.GetRepository();
                var bodyStylesRepo = BodyStylesRepositoryFactory.GetRepository();
                var transmissionReop = TransmissionRepositoryFactory.GetRepository();
                var exteriorColor = ExteriorColorsRepositoryFactory.GetRepository();
                var interior = InteriorRepositoryFactory.GetRepository();

                model.Vehicle = repo.GetVehicleToEdit(model.CurrentVinNumber);
                model.Makes = makesRepo.GetAll();
                model.Models = modelsRepo.GetAll();
                model.VehicleTypes = vehichleType.GetAll();
                model.BodyStyles = bodyStylesRepo.GetAll();
                model.Transmissionns = transmissionReop.GetAll();
                model.ExteriorColors = exteriorColor.GetAll();
                model.Interiors = interior.GetAll();
                model.CurrentVinNumber = model.Vehicle.VinNumber;

                return View(model);
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult DeleteVehicle(int vehicleId, string ImageFileName)
        {
            var repo = VehiclesRepositoryFactory.GetRepository();

            var savePath = Server.MapPath("~/Images");

            var oldImage = Path.Combine(savePath, ImageFileName);

            if (System.IO.File.Exists(oldImage))
            {
                System.IO.File.Delete(oldImage);
            }

            repo.DeleteVehicle(vehicleId);

            return RedirectToAction("Vehicles");
        }

        [Authorize(Roles = "admin")]
        public ActionResult UserList()
        {
            var Db = new ApplicationDbContext();                           
            var modelList = new List<UserItem>();

            var userStore = new UserStore<ApplicationUser>(Db);

            foreach (var user in userStore.Users)
            {
                var userItem = new UserItem();

                userItem.LastName = user.LastName;
                userItem.FirstName = user.FirstName;
                userItem.Email = user.Email;

                userItem.RoleList = UserManager.GetRoles(user.Id);

                userItem.Role = userItem.RoleList.FirstOrDefault();

                modelList.Add(userItem);
            }            

            return View(modelList);
        }

        // GET: Admin
        [Authorize(Roles = "admin")]
        public ActionResult CreateUser()
        {            
            var Db = new ApplicationDbContext();
            var roles = Db.Roles;

            var model = new AddUserViewModel();            
            var roleList = new List<RoleItem>();

            foreach (var role in roles)
            {
                var roleListToAdd = new RoleItem();
                roleListToAdd.RoleId = role.Id;
                roleListToAdd.RoleName = role.Name;

                roleList.Add(roleListToAdd);
            }

            model.Roles = roleList;

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateUser(AddUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName };                

                var result = UserManager.CreateAsync(user, model.Password);

                if (!result.IsFaulted)
                {
                    UserManager.AddToRole(user.Id, model.Role);                    

                    return RedirectToAction("EditUser", "Admin", new { email = model.Email});
                }

                else
                {
                    AddErrors(result.Result);
                }
                
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult EditUser(string email)
        {
            var userToEdit = UserManager.FindByEmail(email);

            var Db = new ApplicationDbContext();
            var roles = Db.Roles;

            var model = new EditUserViewModel();
            var roleList = new List<RoleItem>();

            model.FirstName = userToEdit.FirstName;
            model.LastName = userToEdit.LastName;
            model.Email = userToEdit.Email;
            model.CurrentEmail = userToEdit.Email;
            model.Role = UserManager.GetRoles(userToEdit.Id).FirstOrDefault();

            foreach (var role in roles)
            {
                var roleListToAdd = new RoleItem();
                roleListToAdd.RoleId = role.Id;
                roleListToAdd.RoleName = role.Name;

                roleList.Add(roleListToAdd);
            }

            model.Roles = roleList;

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByEmail(model.CurrentEmail);
                var currentRole = UserManager.GetRoles(user.Id).FirstOrDefault();

                user.UserName = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.Email = model.Email;
                UserManager.RemoveFromRole(user.Id, currentRole);

                var passwordToken = UserManager.GeneratePasswordResetToken(user.Id);

                if (!string.IsNullOrEmpty(model.NewPassword))
                {
                    UserManager.ResetPassword(user.Id, passwordToken, model.NewPassword);
                }

                UserManager.AddToRole(user.Id, model.Role);
                UserManager.Update(user);

                return RedirectToAction("UserList");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Authorize(Roles ="admin")]
        public ActionResult Makes()
        {
            var repo = MakesRepositoryFactory.GetRepository();

            var model = new MakeReportViewModel();

            model.MakeList = repo.GetReport();

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Makes(string MakeName)
        {

            if (ModelState.IsValid)
            {
                var newMake = new Make();
                var repo = MakesRepositoryFactory.GetRepository();

                newMake.MakeName = MakeName;
                newMake.CreatedDate = DateTime.UtcNow;
                newMake.UserId = AuthorizeUtilities.GetUserId(this);

                repo.AddMake(newMake);

                return RedirectToAction("Makes");
            }
            else
            {
                var repo = MakesRepositoryFactory.GetRepository();

                var model = new MakeReportViewModel();

                model.MakeList = repo.GetReport();

                return View(model);
            }
            
        }


        [Authorize(Roles = "admin")]
        public ActionResult Models()
        {
            var modelRepo = ModelsRepositoryFactory.GetRepository();
            var makeRepo = MakesRepositoryFactory.GetRepository();

            var model = new ModelReportViewModel();

            model.ModelList = modelRepo.GetReport();
            model.Makes = makeRepo.GetAll();

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Models(string ModelName, int makeId)
        {
            if (ModelState.IsValid)
            {
                var newModel = new Model();
                var repo = ModelsRepositoryFactory.GetRepository();

                newModel.CreatedDate = DateTime.UtcNow;
                newModel.MakeId = makeId;
                newModel.UserId = AuthorizeUtilities.GetUserId(this);
                newModel.ModelName = ModelName;

                repo.AddModel(newModel);

                return RedirectToAction("Models");
            }
            else
            {
                var modelRepo = ModelsRepositoryFactory.GetRepository();
                var makeRepo = MakesRepositoryFactory.GetRepository();

                var model = new ModelReportViewModel();

                model.ModelList = modelRepo.GetReport();
                model.Makes = makeRepo.GetAll();

                return View(model);
            }
        }


        [Authorize(Roles = "admin")]
        public ActionResult Specials()
        {
            var specials = SpecialsRepositoryFactory.GetRepository().GetAll();

            var model = new SpecialAdminViewModel();

            model.SpecialList = specials;

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Specials(SpecialAdminViewModel SpecialModel)
        {
            if (ModelState.IsValid)
            {
                var newModel = new Special();
                var repo = SpecialsRepositoryFactory.GetRepository();

                if (SpecialModel.ImageUpload != null && SpecialModel.ImageUpload.ContentLength > 0)
                {

                    var savePath = Server.MapPath("~/Images");

                    string extention = Path.GetExtension(SpecialModel.ImageUpload.FileName);

                    var filePath = Path.Combine(savePath, SpecialModel.Special.SpecialName + "-" + DateTime.UtcNow.ToString("yyMMdd") + extention);

                    SpecialModel.ImageUpload.SaveAs(filePath);

                    newModel.SpecialImageFileName = Path.GetFileName(filePath);                    
                }

                newModel.SpecialName = SpecialModel.Special.SpecialName;
                newModel.SpecialDescription = SpecialModel.Special.SpecialDescription;

                repo.AddSpecial(newModel);

                return RedirectToAction("Specials");
            }
            else
            {
                var specials = SpecialsRepositoryFactory.GetRepository().GetAll();

                var model = new SpecialAdminViewModel();

                model.SpecialList = specials;

                return View(model);
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult DeletesSpecial(int SpecialId, string ImageName)
        {
            var repo = SpecialsRepositoryFactory.GetRepository();

            if (ModelState.IsValid)
            {

                var imageSavePath = Server.MapPath("~/Images");

                var filepath = Path.Combine(imageSavePath, ImageName);

                System.IO.File.Delete(filepath);

                repo.DeleteSpecial(SpecialId);

                return RedirectToAction("Specials");
            }
            else
            {
                return RedirectToAction("Specials");
            }

            
        }

        [Authorize(Roles = "admin")]
        public ActionResult Reports()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult InventoryReport()
        {
            var repo = ReportsRepositoryFactory.GetRepository();
            var model = new InventoryReportsViewModel();

            model.NewVehicleReportItem = repo.NewVehicleReport();
            model.UsedVehicleReportItem = repo.UsedVehicleReport();

            return View(model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult SalesReport()
        {
            var Db = new ApplicationDbContext();
            var modelList = new List<SalesReportUserItem>();

            var userStore = new UserStore<ApplicationUser>(Db);

            foreach (var user in userStore.Users)
            {
                var userItem = new SalesReportUserItem();

                userItem.Name = user.FirstName + " " + user.LastName;
                userItem.UserId = user.Id;

                modelList.Add(userItem);
            }

            return View(modelList);

        }

        [Authorize(Roles = "admin,sales")]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin,sales")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }

                return RedirectToAction("ChangePassword", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }
    }
}