﻿using GuildCars.Data.Factories;
using GuildCars.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildCars.UI.Controllers
{
    public class InventoryController : Controller
    {
        

        // GET: Inventory
        public ActionResult Details(string vinNumber)
        {
            var vehichleRepo = VehiclesRepositoryFactory.GetRepository();
            var vehichleModel = vehichleRepo.GetDetails(vinNumber);

            return View(vehichleModel);
        }
        
        public ActionResult New()
        {
            var model = Utilities.InventorySearchUtilities.searchModelValues();

            return View(model);
        }

        public ActionResult Used()
        {
            var model = Utilities.InventorySearchUtilities.searchModelValues();

            return View(model);
        }

    }
}