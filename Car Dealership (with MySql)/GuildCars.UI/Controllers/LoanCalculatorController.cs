﻿using GuildCars.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildCars.UI.Controllers
{
    public class LoanCalculatorController : Controller
    {
        // GET: LoanCalculator
        public ActionResult LoanCalculatorIndex()
        {
            var model = new LoanCalculatorViewModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult LoanCalculatorIndex(LoanCalculatorViewModel LoanValues)
        {
            var model = new LoanCalculatorViewModel();

            double paymentsPerYear = 12;

            model.InterestRate = LoanValues.InterestRate / 100;
            model.LoanLengthInMonths = LoanValues.LoanLengthInMonths;
            model.LoanPrincipal = LoanValues.LoanPrincipal;
            model.LoanLengthInYears = model.LoanLengthInMonths / paymentsPerYear;            

            double loanMonthlyPayment = (model.LoanPrincipal * (model.InterestRate / paymentsPerYear)) / (1 - Math.Pow(1 + (model.InterestRate / paymentsPerYear), -paymentsPerYear * model.LoanLengthInYears));

            model.TotalLoanAmount = loanMonthlyPayment * model.LoanLengthInMonths;
            model.TotalInterestPaid = model.TotalLoanAmount - model.LoanPrincipal;

            loanMonthlyPayment = Math.Round(loanMonthlyPayment, 2);
            model.TotalLoanAmount = Math.Round(model.TotalLoanAmount, 2);
            model.TotalInterestPaid = Math.Round(model.TotalInterestPaid, 2);

            model.MonthlyPayments = loanMonthlyPayment;

            //compare equations
            model.CompareInterestRate = LoanValues.CompareInterestRate / 100;
            model.CompareLoanLengthInMonths = LoanValues.CompareLoanLengthInMonths;
            model.CompareLoanPrincipal = LoanValues.CompareLoanPrincipal;
            model.CompareLoanLengthInYears = model.CompareLoanLengthInMonths / paymentsPerYear;

            double compareLoanMonthlyPayment = (model.CompareLoanPrincipal * (model.CompareInterestRate / paymentsPerYear)) / (1 - Math.Pow(1 + (model.CompareInterestRate / paymentsPerYear), -paymentsPerYear * model.CompareLoanLengthInYears));

            model.CompareTotalLoanAmount = compareLoanMonthlyPayment * model.CompareLoanLengthInMonths;
            model.CompareTotalInterestPaid = model.CompareTotalLoanAmount - model.CompareLoanPrincipal;

            compareLoanMonthlyPayment = Math.Round(compareLoanMonthlyPayment, 2);
            model.CompareTotalLoanAmount = Math.Round(model.CompareTotalLoanAmount, 2);
            model.CompareTotalInterestPaid = Math.Round(model.CompareTotalInterestPaid, 2);

            model.CompareMonthlyPayments = compareLoanMonthlyPayment;

            //difference
            model.DiffMonthlyPayments = Math.Abs(model.MonthlyPayments - model.CompareMonthlyPayments);
            model.DiffTotalLoanAmount = Math.Abs(model.TotalLoanAmount - model.CompareTotalLoanAmount);
            model.DiffTotalInterestPaid = Math.Abs(model.TotalInterestPaid - model.CompareTotalInterestPaid);
            
            return View(model);
        }
    }
}