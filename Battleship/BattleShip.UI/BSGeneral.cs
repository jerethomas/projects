﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.UI
{
    public class BSGeneral
    {
        private string _player1Name;
        private string _player2Name;

        private Board _player1Board = new Board();
        private Board _player2Board = new Board();

        private bool _isPlayer1Turn = true;
        bool victory = false;

        private Display _gameMessage = new Display();
        private Input _gameinput = new Input();

        public void BeginGame()
        {
            _gameMessage.StartMessage();

            _player1Name = _gameinput.GetPlayerName(1);
            _player2Name = _gameinput.GetPlayerName(2);

            while (_player1Name == _player2Name)
            {
                Console.WriteLine("Player names can not be the same.");
                Console.Write("Press any key to enter new names...");
                Console.ReadKey();
                _player1Name = _gameinput.GetPlayerName(1);
                _player2Name = _gameinput.GetPlayerName(2);
            }

            CreateBoards();
            startShooting();

        }



        private void CreateBoards()
        {
            
            foreach (ShipType type in Enum.GetValues(typeof(ShipType)))
            {
                GetShipPosition(_player1Name, _player1Board, type);              
            }

            foreach (ShipType type in Enum.GetValues(typeof(ShipType)))
            {
                GetShipPosition(_player2Name, _player2Board, type);               
            }

        }

        private void GetShipPosition(string playerName, Board board, ShipType type)
        {
            while (true)
            {
                
                _gameMessage.DisplayGrid(board);

                int shipSize = 0;

                PlaceShipRequest shipPosition = new PlaceShipRequest();

                switch (type)
                {
                    case ShipType.Destroyer:
                        shipSize = 2;
                        break;
                    case ShipType.Cruiser:
                        shipSize = 3;
                        break;
                    case ShipType.Submarine:
                        shipSize = 3;
                        break;
                    case ShipType.Battleship:
                        shipSize = 4;
                        break;
                    case ShipType.Carrier:
                        shipSize = 5;
                        break;
                }

                Console.Write($"{playerName}, you are about to place your {type}, which is {shipSize} squares big. ");
                Coordinate coord = _gameinput.GetCoordinate();
                ShipDirection direction = _gameinput.GetShipDirection();

                shipPosition.Coordinate = coord;
                shipPosition.Direction = direction;
                shipPosition.ShipType = type;

                ShipPlacement placement = board.PlaceShip(shipPosition);

                switch (placement)
                {
                    case ShipPlacement.NotEnoughSpace:
                        Console.Write("There is not enough room in that spot of the board. Press any key to try again.");
                        Console.ReadLine();
                        break;
                    case ShipPlacement.Ok:
                        return;
                    case ShipPlacement.Overlap:
                        Console.Write("That spot is taken by another ship. Press any key to try again.");
                        Console.ReadLine();
                        break;
                }
            }

        }

        private void startShooting()
        {

            while (!victory)
            {
                if (_isPlayer1Turn == true)
                {
                    getPlayerShot(_player1Name, _player2Board);
                    _isPlayer1Turn = false;
                }
                else
                {
                    getPlayerShot(_player2Name, _player1Board);
                    _isPlayer1Turn = true;
                }
            }

            if (victory == true)
            {
                Console.Clear();
                
            }

        }

        private void getPlayerShot(string playerName, Board board)
        {
            bool invalidInput = true;
            while (invalidInput)
            {
                _gameMessage.DisplayGrid(board);

                Console.Write($"{playerName}, time to shoot. ");
                Coordinate coord = _gameinput.GetCoordinate();
                FireShotResponse response = board.FireShot(coord);

                switch (response.ShotStatus)
                {
                    case ShotStatus.Invalid:
                        Console.Write("That was an invalid shot location. Press any key to fire again");
                        Console.ReadKey();
                        break;
                    case ShotStatus.Duplicate:
                        Console.Write("You already shot there. Press any key to fire again.");
                        Console.ReadKey();
                        break;
                    case ShotStatus.Miss:
                        _gameMessage.DisplayGrid(board);
                        Console.Write("That shot missed. Press any key to continue...");
                        Console.ReadKey();
                        invalidInput = false;
                        break;
                    case ShotStatus.Hit:
                        _gameMessage.DisplayGrid(board);
                        Console.Write("That shot was a hit! Press any key to continue...");
                        Console.ReadKey();
                        invalidInput = false;
                        break;
                    case ShotStatus.HitAndSunk:
                        _gameMessage.DisplayGrid(board);
                        Console.Write("That shot hit and sunk! Press any key to continue...");                        
                        Console.ReadKey();
                        invalidInput = false;
                        break;
                    case ShotStatus.Victory:
                        _gameMessage.DisplayGrid(board);
                        Console.Write($"That shot sunk the final ship! {playerName} is the winnner!");
                        Console.Write("Would you like to play again? Y for yes, anthing else for no: ");
                        string Input = Console.ReadLine().ToUpper();

                        if ((Input == "Y") || (Input == "YES"))
                        {
                            BeginGame();
                        }

                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Thank you for playing. Press any key to quit.");
                            Console.ReadKey();
                            victory = true;
                        }
                        
                        invalidInput = false;                        
                        break;
                    default:
                        break;
                }
            }
 
        }

    }
}

